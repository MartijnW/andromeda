<?php

use Andromeda\Util\IOUtil;

ini_set('display_errors', true);
error_reporting(E_ALL);

require_once __DIR__ . '/../Andromeda/Util/IOUtil.php';

$search_dir = 'J:\xampp';

echo '<h3>Directories:</h3>';
foreach (IOUtil::getDirectories($search_dir) as $directory)
	dump($directory);

echo '<h3>Files:</h3>';
foreach (IOUtil::getFiles($search_dir) as $file)
	dump($file);

function dump($obj)
{
	echo '<pre>' . var_export($obj, true) . '</pre>';
}