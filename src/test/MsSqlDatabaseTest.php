<?php
declare(strict_types=1);

use Andromeda\Database\DatabaseModel;
use Andromeda\Database\MsSql\MsSqlCredentials;
use Andromeda\Database\MsSql\MsSqlManager;

error_reporting(E_ALL);
ini_set('display_errors', 'true');

require_once __DIR__ . '/Autoloader.php';
$al = new Autoloader();
$al->addDirectory(__DIR__ . '/../Andromeda', 'Andromeda\\', true);
$al->register();

class MsSqlDatabase extends MsSqlManager
{
	/**
	 * BiemBase constructor.
	 */
	public function __construct()
	{
		// https://gearhost.com
//		$credentials = new MsSqlCredentials('den1.mssql7.gear.host', 'dbmandromeda', 'dbmandromeda', 'Qm957QP-!8LZ');
		$credentials = new MsSqlCredentials('mtun.es', 'TestDb', 'SA', 'c2vUc0swuzeswob', MsSqlCredentials::CONNECTION_TYPE_DBLIB);

		parent::__construct($credentials);
	}
}

class UserModel extends DatabaseModel
{
	public $id;

	public $firstname;

	public $lastname;

	public $email;

	public $gender;

	public $active;
}

$version = MsSqlDatabase::select('@@VERSION')->fetch();
debug('Version:', $version);

$plain_user = MsSqlDatabase::select()->from('user')->fetch();
debug('Plain user:', $plain_user);

$user = MsSqlDatabase::select()->from('user')->fetchInto(UserModel::class);
debug('First user:', $user);

$two_users = MsSqlDatabase::select()->from('user')->limit(2)->fetchAllInto(UserModel::class);
debug('First 2 users:', $two_users);

$two_offset_users = MsSqlDatabase::select()->orderBy('id', 'ASC')->limit(2)->from('user')->offset(1)->fetchAll();
debug('Second two users:', $two_offset_users);

$bind = MsSqlDatabase::select()->from('user')->where('lastname', 'LIKE', ':l')->bind(':l', 'smit')->fetch();
debug('Binding query:', $bind);

$bind_multiple = MsSqlDatabase::select()
	->from('user')
	->where('lastname', '=', ':l')
	->where('firstname', 'LIKE', ':f')
	->bind(':l', 'smit')
	->bind(':f', 'hans')
	->fetchAll();
debug('Binding multiple query:', $bind_multiple);

$or = MsSqlDatabase::select()
	->from('user')
	->where('firstname', '=', ':f1')
	->orWhere('firstname', '=', ':f2')
	->bind(':f1', 'hans')
	->bind(':f2', 'ana')
	->fetchAll();
debug('Or query:', $or);

$left_join = MsSqlDatabase::select('u.*', 'up.phone')
	->from('user', 'u')
	->leftJoin('user_phone', 'up', 'up.user_id = u.id')
	->where('u.lastname', '=', ':l')
	->bind(':l', 'smit')
	->fetchAll();
debug('Left join query:', $left_join);

$get_query = MsSqlDatabase::select('firstname', 'lastname')
	->from('user')
	->where('firstname', '=', ':a')
	->bind(':a', 'ana');
debug('Query:', $get_query->__toString());

$group_by = MsSqlDatabase::select('count(*) as count', 'firstname')
	->from('user')
	->groupBy('firstname')
	->fetchAll();
debug('Group by query:', $group_by);

//$binds = [
//	':f' => 'SQL',
//	':l' => 'Manager',
//	':e' => 'sql.manager@insert.com',
//	':g' => 2,
//	':a' => true
//];
//
//$insert = MsSqlDatabase::insertInto('user', 'firstname', 'lastname', 'email', 'gender', 'active')
//	->values(':f', ':l', ':e', ':g', ':a')
//	->bindAll($binds)
//	->execute();
//debug('Insert query:', $insert);
//
//$update = MsSqlDatabase::update('user')
//	->set('email', ':e')
//	->set('active', ':a')
//	->where('firstname', '=', ':f')
//	->where('lastname', '=', ':l')
//	->bind(':e', 'sql.manager@update.com')
//	->bind(':a', false)
//	->bind(':f', 'SQL')
//	->bind(':l', 'Manager')
//	->execute();
//debug('Update query:', $update);

$manual_query = MsSqlDatabase::query('SELECT @@VERSION')->fetch();
debug('Manual query:', $manual_query);

$union_query = MsSqlDatabase::select()->from('user')->where('firstname', '=', ':f1')->bind(':f1', 'Ana');
$union = MsSqlDatabase::select()->from('user')->where('lastname', '=', ':l1')->bind(':l1', 'Smit')->unionAll($union_query)->fetchAll();
debug('Union query:', $union);

$with_query = MsSqlDatabase::select()->from('user')->limit(1);
$cte = MsSqlDatabase::select()->from('myQuery')->with('myQuery', $with_query)->__toString();
debug('CTE query:', $cte);

$with_query2 = MsSqlDatabase::select()->from('user')->limit(1);
$with_query3 = MsSqlDatabase::select()->from('user')->where('firstname', '=', ':f1')->bind(':f1', 'Ana');
$union = MsSqlDatabase::select()->from('mySecondQuery');

$cte2 = MsSqlDatabase::select()
	->with('myQuery', MsSqlDatabase::select()
		->from('user')
		->limit(1))
	->with('mySecondQuery', MsSqlDatabase::select()
		->from('user')
		->where('firstname', '=', ':f1')
		->bind(':f1', 'Ana'))
	->from('myQuery')
	->unionAll(MsSqlDatabase::select()
		->from('mySecondQuery'))
	->fetchAll();

debug('Multiple CTE query:', $cte2);

//$data = [
//	['SqlBatch', 'Manager', 'sqlbatch@manager.com', 1, true],
//	['BatchSQL', 'Manager', 'batchsql@manager.com', 2, false]
//];
//
//$batch_insert = MsSqlDatabase::insertInto('user', 'firstname', 'lastname', 'email', 'gender', 'active')
//	->valuesBatch(...$data)
//	->execute();
//debug('Batch insert:', $batch_insert);

$top_percent = MsSqlDatabase::select()
	->from('user')
	->limit(50, MsSqlDatabase::TOP_PERCENT)
	->fetchAll();
debug('Top percent:', $top_percent);

$error_query = MsSqlDatabase::select('unknown')
	->from('user')
	->fetch();
debug('Error query result:', ['query_result' => $error_query, 'error_info' => MsSqlDatabase::errorInfo()->getMessage() ?? 'Could not get error message.']);

function debug(string $message, $obj)
{
	echo "<pre>$message\r\n" . var_export($obj, true) . '</pre>';
}