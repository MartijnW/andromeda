<?php

use Andromeda\Data\Curl\CurlRequest;
use Andromeda\Util\CurlUtil;

ini_set('display_errors', true);
error_reporting(E_ALL);

require_once __DIR__ . '/Autoloader.php';
$al = new Autoloader();
$al->addDirectory(__DIR__ . '/../Andromeda', 'Andromeda\\', true);
$al->register();

$get_url = 'https://httpbin.org/get';
$post_url = 'https://httpbin.org/post';
$put_url = 'https://httpbin.org/put';
$delete_url = 'https://httpbin.org/delete';
$patch_url = 'https://httpbin.org/patch';

$curl = new CurlRequest();

echo '<h3>Get request</h3>';
$get_response = $curl->get($get_url);
dumpResult($get_response);

echo '<h3>Post request</h3>';
$post_response = $curl->post(['test' => 'a', 'b'], $post_url);
dumpResult($post_response);

echo '<h3>Put request</h3>';
$put_response = $curl->put(['test' => 'a', 'b'], $put_url);
dumpResult($put_response);

echo '<h3>Delete request</h3>';
$delete_response = $curl->delete($delete_url);
dumpResult($delete_response);

echo '<h3>Patch request</h3>';
$path_response = $curl->patch(['a' => 'b', 'c' => 1], $patch_url);
dumpResult($path_response);

echo '<h3>---------------------------</h3>';
echo '<h3>Other way of doing it</h3>';
dumpResult((new CurlRequest())->setUrl('https://httpbin.org/anything')->addHeader('Authorization: Basic test:test')->get());
dumpResult((new CurlRequest())->addHeader('Authorization: Basic test:test')->post(['Hi, I am post data'], 'https://httpbin.org/anything'));


echo '<h3>---------------------------</h3>';
echo '<h3>Multicurl</h3>';
$request1 = (new CurlRequest())->setGet()->setUrl('https://mtun.es/download/fade.txt')->setCallback('dumpResult');
$request2 = (new CurlRequest())->setGet()->setUrl('https://mtun.es/download/nexus.txt')->setCallback('dumpResult');
CurlUtil::batch($request1, $request2);

function dumpResult($result)
{
	if (is_string($result))
		echo '<pre>' . htmlspecialchars($result) . '</pre>';
	else
		echo '<pre>' . var_export($result, true) . '</pre>';
}