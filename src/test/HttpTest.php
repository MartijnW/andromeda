<?php
declare(strict_types=1);

use Andromeda\Util\HttpCodes;

error_reporting(E_ALL);
ini_set('display_errors', 'true');

require_once __DIR__ . '/Autoloader.php';
$al = new Autoloader();
$al->addDirectory(__DIR__ . '/../Andromeda', 'Andromeda\\', true);
$al->register();

debug('Ok code:', HttpCodes::OK);
debug('Ok name:', HttpCodes::getName(HttpCodes::OK));
debug('HTTP not supported name:', HttpCodes::getName(505));
debug('Invalid HTTP Code name:', HttpCodes::getName(-123));
debug('Cloudflare Unkown:', HttpCodes::CLOUDFLARE_UNKNOWN_ERROR);
debug('Cloudflare timeout name:', HttpCodes::getName(HttpCodes::CLOUDFLARE_A_TIMEOUT_OCCURRED));

function debug(string $message, $obj)
{
	echo "<pre>$message\r\n" . var_export($obj, true) . '</pre>';
}