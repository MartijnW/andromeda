<?php
declare(strict_types=1);

use Andromeda\Database\DatabaseModel;
use Andromeda\Database\MySql\MySqlCredentials;
use Andromeda\Database\MySql\MySqlManager;

error_reporting(E_ALL);
ini_set('display_errors', 'true');

require_once __DIR__ . '/Autoloader.php';
$al = new Autoloader();
$al->addDirectory(__DIR__ . '/../Andromeda', 'Andromeda\\', true);
$al->register();

class BiemBase extends MySqlManager
{
	/**
	 * BiemBase constructor.
	 */
	public function __construct()
	{
		$credentials = new MySqlCredentials('db4free.net', 'biem_base', 'biem_user', 'in_de_water');

		parent::__construct($credentials, true);
	}
}

class UserModel extends DatabaseModel
{
	public $id;

	public $firstname;

	public $lastname;

	public $email;

	public $gender;

	public $active;
}

$version = BiemBase::select('VERSION()')->fetch();
debug('Version:', $version);

$user = BiemBase::select()->from('user')->fetchInto(UserModel::class);
debug('First user:', $user);

$two_users = Biembase::select()->from('user')->limit(2)->fetchAllInto(UserModel::class);
debug('First 2 users:', $two_users);

$bind = Biembase::select()->from('user')->where('firstname', 'LIKE', ':l')->bind(':l', 'hans')->fetch();
debug('Binding query:', $bind);

$union_query = BiemBase::select()->from('user')->where('firstname', '=', ':f1')->bind(':f1', 'ana');
$union = BiemBase::select()
	->from('user')
	->where('firstname', '=', ':f2')
	->bind(':f2', 'hans')
	->unionAll($union_query)
	->fetchAll();
debug('Union query:', $union);

function debug(string $message, $obj)
{
	echo "<pre>$message\r\n" . var_export($obj, true) . '</pre>';
}