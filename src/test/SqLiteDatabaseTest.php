<?php
declare(strict_types=1);

use Andromeda\Database\SqLite\SqLiteManager;

error_reporting(E_ALL);
ini_set('display_errors', 'true');

require_once __DIR__ . '/Autoloader.php';
$al = new Autoloader();
$al->addDirectory(__DIR__ . '/../Andromeda', 'Andromeda\\', true);
$al->register();

class TestSqLite extends SqLiteManager
{
	/**
	 * BiemBase constructor.
	 */
	public function __construct()
	{
		try
		{
			parent::__construct(__DIR__ . '/TestDatabase', true);
		}
		catch (Exception $ex)
		{
			debug('ERROR: ', $ex);
		}
	}
}

$simple = TestSqLite::select('1 as first')->fetch();
debug('Simple:', $simple);

$first_record = TestSqLite::select()->from('user')->fetch();
debug('First:', $first_record);

$rowcount = TestSqLite::select()->from('user')->rowCount();
debug('Rowcount', $rowcount);

$bind = TestSqLite::select()->from('user')->where('firstname', 'LIKE', ':l')->bind(':l', 'hans')->fetch();
debug('Bind:', $bind);

$isTest = TestSqLite::select()->from('user')->where('firstname', '=', ':f')->bind(':f', 'Hans')->fetch();
debug('Is:', $isTest);

$isNotTest = TestSqLite::select()->from('user')->where('firstname', '<>', ':f')->bind(':f', 'Hans')->fetch();
debug('IsNot:', $isNotTest);

$likeTest = TestSqLite::select()->from('user')->where('firstname', 'like', ':f')->bind(':f', '%ana%')->fetch();
debug('Like:', $likeTest);

$notLikeTest = TestSqLite::select()->from('user')->where('firstname', 'not like', ':f')->bind(':f', '%ana%')->fetch();
debug('NotLike:', $notLikeTest);

$union_test = TestSqLite::select()
	->from('user')
	->where('firstname', '=', ':f1')
	->bind(':f1', 'Hans')
	->unionAll(TestSqLite::select()
		->from('user')
		->where('firstname', '=', ':f2')
		->bind(':f2', 'Ana'))
	->fetchAll();
debug('Union:', $union_test);

$cte = TestSqLite::select()
	->with('Cte', TestSqLite::select()->from('user'))
	->from('Cte')
	->fetchAll();
debug('CTE:', $cte);

function debug(string $message, $obj)
{
	echo "<pre>$message\r\n" . var_export($obj, true) . '</pre><br/>';
}