<?php
declare(strict_types=1);

use Andromeda\Synology\Apis\FileStation\DownloadRequestModel;
use Andromeda\Synology\Apis\FileStation\GetDirectoriesRequestModel;
use Andromeda\Synology\Apis\FileStation\RenameRequestModel;
use Andromeda\Synology\Apis\FileStation\UploadRequestModel;
use Andromeda\Synology\DiskStation;
use Andromeda\Synology\SynologyCredentials;

error_reporting(E_ALL);
ini_set('display_errors', 'true');

require_once __DIR__ . '/Autoloader.php';
$al = new Autoloader();
$al->addDirectory(__DIR__ . '/../Andromeda', 'Andromeda\\', true);
$al->register();

$ds = new DiskStation(new SynologyCredentials('xxxxxxx', 5001, true, 'xxxxxxx', 'xxxxxxx', true));

echo '<p>FileStation Info:</p>';
var_dump($ds->getFileStation()->getInfo());
echo '</br>';

echo '<p>Available APIs:</p>';
var_dump($ds->getAvailableApis());
echo '<br />';

echo '<p>FileStation Directories:</p>';
$model = new GetDirectoriesRequestModel();
$model->setSortDirection(GetDirectoriesRequestModel::SORT_BY_CHANGE_TIME);
var_dump($ds->getFileStation()->getDirectories($model));
echo '</br>';

echo '<p>FileStation Upload:</p>';
$upload_request = new UploadRequestModel();
$upload_request->setUploadFile('/home/pi/Documents/UploadTest.txt');
$upload_request->setOverwrite(true);
$upload_request->setTargetPath('/Downloads/Test');
//$upload_request->setUploadFile('/home/pi/Documents/uta.mp4');

var_dump($ds->getFileStation()->upload($upload_request));

echo '<p>FileStation Rename:</p>';
$rename_request = new RenameRequestModel(['/Downloads/Test/a.txt', '/Downloads/Test/b.txt', '/Downloads/Test/c.txt'],
	['one.txt', 'two.txt', 'three.txt']);

var_dump($ds->getFileStation()->rename($rename_request));

echo '<p>FileStation Download:</p>';
try
{
	$download_request = new DownloadRequestModel('/Downloads/Test/uta.mp4', '/home/pi/Documents/uta1.mp4', true);
	var_dump($ds->getFileStation()->download($download_request));
}
catch (Exception $e)
{
	echo $e;
}

$ds->logout();