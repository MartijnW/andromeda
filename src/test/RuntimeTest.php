<?php
declare(strict_types=1);

use Andromeda\Util\RuntimeUtil;

require_once __DIR__ . '/../Andromeda/Util/RuntimeUtil.php';

debug('Is Windows:', RuntimeUtil::isWindows());
debug('Is Linux:', RuntimeUtil::isLinux());
debug('Is MacOS:', RuntimeUtil::isMacOS());

function debug(string $message, $obj)
{
	echo "<pre>$message\r\n" . var_export($obj, true) . '</pre>';
}