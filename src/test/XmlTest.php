<?php

use Andromeda\Data\Xml\XmlAttribute;
use Andromeda\Data\Xml\XmlElement;
use Andromeda\Data\Xml\XmlDocument;

require_once __DIR__ . '/../Andromeda/Data/Xml/XmlAttribute.php';
require_once __DIR__ . '/../Andromeda/Data/Xml/XmlElement.php';
require_once __DIR__ . '/../Andromeda/Data/Xml/XmlDocument.php';

$helper = new XmlDocument();

$attr1 = new XmlAttribute();
$attr1->name = 'id';
$attr1->value = 123;

$attr2 = new XmlAttribute();
$attr2->name = 'street';
$attr2->value = 'MyStreet';

$attr3 = new XmlAttribute();
$attr3->name = 'Firstname';
$attr3->value = 'Bas';

$attr4 = new XmlAttribute();
$attr4->name = 'Lastname';
$attr4->value = 'Milius';

$elem1 = new XmlElement();
$elem1->name = 'Test';
$elem1->attributes = $attr1;

$elem2 = new XmlElement();
$elem2->name = 'Person';
$elem2->attributes = [$attr3, $attr4];

$elem3 = new XmlElement();
$elem3->name = 'Address';
$elem3->attributes = $attr2;

$elem2->data = $elem3;
$elem1->data = $elem2;

$helper->addElement($elem1);

debug('JSON:', $helper->toJson());

$xml = $helper->getXml();
debug('Output XML:', htmlspecialchars($xml));

$helper2 = new XmlDocument();
$json_data = [
	'Item1' => 'blub',
	'Foo' => 'bar',
	'Persons' => [
		['Firstname' => 'test'],
		['Firstname' => 'Hello']
	],
];

debug('JSON -> XML:', htmlspecialchars($helper2->fromJson('RootElem', $json_data)->getXml()));

function debug(string $message, $obj)
{
	echo "<pre>$message\r\n" . var_export($obj, true) . '</pre>';
}