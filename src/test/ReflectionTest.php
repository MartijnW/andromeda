<?php
declare(strict_types=1);

use Andromeda\Util\ReflectionUtil;

require_once __DIR__ . '/../Andromeda/Util/StringUtil.php';
require_once __DIR__ . '/../Andromeda/Util/ReflectionUtil.php';

/**
 * Class ReflectionTest
 *
 * @test   A
 * @test   B
 * @arr    1
 * @arr    two
 * @arr    this is three
 * @author MartijnW
 */
class ReflectionTest
{
	/**
	 * @var string
	 */
	public $property;

	/**
	 * @authenticate
	 * @author MartijnW
	 * @return array
	 */
	public function method() : array
	{
		return ['a' => 1];
	}
}

/**
 * @author MartijnW
 */
function reflection_function()
{

}

debug('Class attributes:', ReflectionUtil::getClassAttributes(ReflectionTest::class));
debug('Method attributes:', ReflectionUtil::getMethodAttributes(ReflectionTest::class, 'method'));
debug('Property attributes:', ReflectionUtil::getPropertyAttributes(ReflectionTest::class, 'property'));
debug('Function attributes:', ReflectionUtil::getFunctionAttributes('reflection_function'));


function debug(string $message, $obj)
{
	echo "<pre>$message\r\n" . var_export($obj, true) . '</pre>';
}