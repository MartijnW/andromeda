<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 22/08/2018
 * Time: 16:55
 */

namespace Andromeda\Data\Curl;

/**
 * Class CurlRequest
 *
 * @since   1.0.1
 * @author  MartijnW
 * @package Andromeda\Data\Curl
 */
class CurlRequest
{
	private const OPTIONS = [
		CURLOPT_FOLLOWLOCATION => true,
		CURLOPT_TIMEOUT => 10,
		CURLOPT_RETURNTRANSFER => true
	];

	private const HTTP_METHODS = [
		'GET' => 'GET',
		'POST' => 'POST',
		'PUT' => 'PUT',
		'DELETE' => 'DELETE',
		'PATCH' => 'PATCH'
	];

	/** @var resource */
	protected $curl;

	/** @var callable */
	protected $callback;

	/** @var array */
	private $options;

	/** @var string[] */
	private $headers;

	/** @var array */
	private $debugInfo;

	/**
	 * CurlRequest constructor.
	 *
	 * @since 1.0.1
	 * @param array $options
	 */
	public function __construct(array $options = self::OPTIONS)
	{
		$this->options = $options;
		$this->init();
	}

	/**
	 * Sets the target URL
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param string $url
	 * @return CurlRequest
	 */
	public function setUrl(string $url) : self
	{
		$this->setOption(CURLOPT_URL, $url);

		return $this;
	}

	/**
	 * Performs a GET request
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param null|string $url
	 * @return null|string
	 */
	public function get(?string $url = null) : ?string
	{
		return $this->getResponse($url);
	}

	/**
	 * Sets the request to be a GET request
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @return CurlRequest
	 */
	public function setGet() : self
	{
		if (isset($this->options[CURLOPT_CUSTOMREQUEST]))
			unset($this->options[CURLOPT_CUSTOMREQUEST]);

		if (isset($this->options[CURLOPT_POST]))
			unset($this->options[CURLOPT_POST]);

		if (isset($this->options[CURLOPT_POSTFIELDS]))
			unset($this->options[CURLOPT_POSTFIELDS]);

		return $this;
	}

	/**
	 * Performs a POST request
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param array       $data
	 * @param null|string $url
	 * @return null|string
	 */
	public function post(array $data = [], ?string $url = null) : ?string
	{
		$this->setOption(CURLOPT_POST, true);
		$this->setOption(CURLOPT_POSTFIELDS, http_build_query($data));

		return $this->getResponse($url);
	}

	/**
	 * Performs a POST request with the data string
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string      $data
	 * @param null|string $url
	 * @return null|string
	 */
	public function postString(string $data, ?string $url = null) : ?string
	{
		$this->setOption(CURLOPT_POST, true);
		$this->setOption(CURLOPT_POSTFIELDS, $data);

		return $this->getResponse($url);
	}

	/**
	 * Performs a POST request with the plain array data
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param array       $data
	 * @param null|string $url
	 * @return null|string
	 */
	public function postFormData(array $data, ?string $url = null) : ?string
	{
		$this->setOption(CURLOPT_POST, true);
		$this->setOption(CURLOPT_POSTFIELDS, $data);

		return $this->getResponse($url);
	}

	/**
	 * Sets the request to be a POST request
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param array $data
	 * @return CurlRequest
	 */
	public function setPost(array $data = []) : self
	{
		if (isset($this->options[CURLOPT_CUSTOMREQUEST]))
			unset($this->options[CURLOPT_CUSTOMREQUEST]);

		$this->setOption(CURLOPT_POST, true);
		$this->setOption(CURLOPT_POSTFIELDS, http_build_query($data));

		return $this;
	}

	/**
	 * Performs a PUT request
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param array       $data
	 * @param null|string $url
	 * @return null|string
	 */
	public function put(array $data = [], ?string $url = null) : ?string
	{
		$this->setOption(CURLOPT_CUSTOMREQUEST, self::HTTP_METHODS['PUT']);
		$this->setOption(CURLOPT_POSTFIELDS, http_build_query($data));

		return $this->getResponse($url);
	}

	/**
	 * Sets the request to be a PUT request
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param array $data
	 * @return CurlRequest
	 */
	public function setPut(array $data = []) : self
	{
		if (isset($this->options[CURLOPT_POST]))
			unset($this->options[CURLOPT_POST]);

		$this->setOption(CURLOPT_CUSTOMREQUEST, self::HTTP_METHODS['PUT']);
		$this->setOption(CURLOPT_POSTFIELDS, http_build_query($data));

		return $this;
	}

	/**
	 * Performs a DELETE request
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param null|string $url
	 * @return null|string
	 */
	public function delete(?string $url = null) : ?string
	{
		$this->setOption(CURLOPT_CUSTOMREQUEST, 'DELETE');

		return $this->getResponse($url);
	}

	/**
	 * Sets the request to be a DELETE request
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @return CurlRequest
	 */
	public function setDelete() : self
	{
		if (isset($this->options[CURLOPT_POST]))
			unset($this->options[CURLOPT_POST]);

		$this->setOption(CURLOPT_CUSTOMREQUEST, self::HTTP_METHODS['DELETE']);

		return $this;
	}

	/**
	 * Performs a PATCH request
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param array       $data
	 * @param null|string $url
	 * @return null|string
	 */
	public function patch(array $data = [], ?string $url = null) : ?string
	{
		$this->setOption(CURLOPT_CUSTOMREQUEST, 'PATCH');
		$this->setOption(CURLOPT_POSTFIELDS, http_build_query($data));

		return $this->getResponse($url);
	}

	/**
	 * Sets the request to be a PATCH request
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param array $data
	 * @return CurlRequest
	 */
	public function setPatch(array $data = []) : self
	{
		if (isset($this->options[CURLOPT_POST]))
			unset($this->options[CURLOPT_POST]);

		$this->setOption(CURLOPT_CUSTOMREQUEST, self::HTTP_METHODS['PATCH']);
		$this->setOption(CURLOPT_POSTFIELDS, http_build_query($data));

		return $this;
	}

	/**
	 * Sets the callback for when the request is executed.
	 * Calls with one parameter (string) that contains the result data.
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param callable $callback
	 * @return CurlRequest
	 */
	public function setCallback(callable $callback) : self
	{
		$this->callback = $callback;

		return $this;
	}

	/**
	 * Sets a specific curl option
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param int $option
	 * @param     $value
	 * @return CurlRequest
	 */
	public function setOption(int $option, $value) : self
	{
		$this->options[$option] = $value;

		return $this;
	}

	/**
	 * Adds a new header to the request
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param string $header
	 * @return CurlRequest
	 */
	public function addHeader(string $header) : self
	{
		$this->headers[] = $header;

		return $this;
	}

	/**
	 * Gets the information about the request (can only be called after request)
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @return array
	 */
	public function getRequestInfo() : array
	{
		return $this->debugInfo ?? [];
	}

	/**
	 * Safly closes the curl resource
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 */
	public function __destruct()
	{
		$this->close();
	}

	/**
	 * Closes the current curl resource
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 */
	private function close() : void
	{
		if ($this->curl === null)
			return;

		curl_close($this->curl);
		$this->curl = null;
	}

	/**
	 * Initializes a new curl resource
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 */
	private function init() : void
	{
		$this->curl = curl_init();
	}

	/**
	 * Prepares curl for executing
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param null|string $url
	 */
	protected function prepare(?string $url) : void
	{
		if ($url !== null)
			$this->setUrl($url);

		if ($this->curl === null)
			$this->init();

		curl_setopt_array($this->curl, $this->options);

		if (!empty($this->headers))
			curl_setopt($this->curl, CURLOPT_HTTPHEADER, $this->headers);
	}

	/**
	 * Gets a response
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param null|string $url
	 * @return null|string
	 */
	private function getResponse(?string $url) : ?string
	{
		$this->prepare($url);

		$response = curl_exec($this->curl);
		$this->debugInfo = curl_getinfo($this->curl);
		$this->close();

		if ($this->callback !== null)
			call_user_func($this->callback, $response);

		return ($response === false ? null : $response);
	}
}