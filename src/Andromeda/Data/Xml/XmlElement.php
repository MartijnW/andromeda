<?php
/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 09/07/2018
 * Time: 22:58
 */

namespace Andromeda\Data\Xml;

class XmlElement
{
	public $name;

	public $attributes;

	public $data;
}