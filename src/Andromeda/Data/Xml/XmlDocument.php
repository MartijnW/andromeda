<?php

namespace Andromeda\Data\Xml;

use Andromeda\Data\Xml\Exception\XmlParseException;
use XMLWriter;

/**
 * Class XmlDocument
 *
 * @author  MartijnW
 * @package Andromeda\Util
 * @since   1.0.1
 */
class XmlDocument extends XMLWriter
{
	private $cachedXml = null;

	public function __construct(string $version = '1.0', string $encoding = 'UTF-8')
	{
		$this->openMemory();
		$this->setIndent(true);
		$this->setIndentString(' ');
		$this->startDocument($version, $encoding);
	}

	public function addElement(XmlElement $element) : self
	{
		$this->startElement($element->name);

		if ($element->attributes !== null)
		{
			if (!is_array($element->attributes))
				$element->attributes = [$element->attributes];

			/** @var XmlAttribute $attribute */
			foreach ($element->attributes as $attribute)
			{
				$this->startAttribute($attribute->name);
				$this->text($attribute->value);
				$this->endAttribute();
			}
		}

		if ($element->data !== null)
		{
			if (is_string($element->data))
				$this->text($element->data);
			else
			{
				if (!is_array($element->data))
					$element->data = [$element->data];

				foreach ($element->data as $item)
				{
					if ($item instanceof XmlElement)
						$this->addElement($item);
				}
			}
		}

		$this->endElement();

		return $this;
	}

	public function addElementByKeyValue(string $key, $value, bool $add = true) : XmlElement
	{
		$model = new XmlElement();
		$model->name = $key;

		if (is_array($value))
		{
			foreach ($value as $name => $obj)
				$model->data[] = $this->addElementByKeyValue($name, $obj, false);
		}
		else
			$model->data = $value;

		if ($add)
			$this->addElement($model);

		return $model;
	}

	public function fromJson(string $root_element_name, array $data) : self
	{
		$this->addElementByKeyValue($root_element_name, $data);

		return $this;
	}

	/**
	 * @since 1.0.1
	 * @return string
	 * @throws XmlParseException
	 */
	public function toJson() : string
	{
		libxml_use_internal_errors(true);

		$xml = simplexml_load_string($this->getXml());
		libxml_use_internal_errors(false);

		if ($xml === false)
			throw new XmlParseException('Could not parse XML to JSON. Are there 2 root objects?');

		return json_encode($xml);
	}

	public function getXml() : string
	{
		if ($this->cachedXml === null)
		{
			$this->endDocument();
			$this->cachedXml = $this->outputMemory();
		}

		return $this->cachedXml;
	}
}