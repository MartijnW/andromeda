<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 03/10/2018
 * Time: 19:25
 */

namespace Andromeda\Synology;

/**
 * Interface IRequestModel
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Synology
 */
interface IRequestModel
{

}