<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 28/09/2018
 * Time: 17:06
 */

namespace Andromeda\Synology;

use Andromeda\Synology\Apis\AudioStation\SynologyAudioStation;
use Andromeda\Synology\Apis\DownloadStation\SynologyDownloadStation;
use Andromeda\Synology\Apis\FileStation\SynologyFileStation;
use Andromeda\Synology\Apis\ResponseModels\AvailableApiResponseModel;

/**
 * Class DiskStation
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Synology
 */
class DiskStation
{
	use SynologyApi;

	/** @var null|SynologyFileStation */
	private $fileStation = null;

	/** @var null|SynologyDownloadStation */
	private $downloadStation = null;

	/** @var null|SynologyAudioStation */
	private $audioStation = null;

	/**
	 * DiskStation constructor.
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param SynologyCredentials $credentials
	 */
	public function __construct(SynologyCredentials $credentials)
	{
		$this->credentials = $credentials;
		$this->allowSelfSignedCertificate = $credentials->allowSelfSignedCertificate;
	}

	/**
	 * Gets all of the available APIs and their versions
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $appIdentifier
	 * @return array<string, AvailableApiResponseModel>|null
	 */
	public function getAvailableApis(string $appIdentifier = 'all') : ?array
	{
		$result = $this->performRequest($this->credentials->getFullHostName() . 'webapi/query.cgi?api=SYNO.API.Info&version=1&method=query&query=' . $appIdentifier);

		if ($result === null)
			return null;

		$decoded_objects = [];
		foreach (json_decode($result, true)['data'] ?? [] as $key => $object)
			$decoded_objects[$key] = $this->decodeInto(AvailableApiResponseModel::class, $object);

		return $decoded_objects;
	}

	/**
	 * Gets the Synology FileStation
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @return SynologyFileStation
	 * @throws \Exception
	 */
	public function getFileStation() : SynologyFileStation
	{
		if ($this->fileStation === null)
			$this->fileStation = new SynologyFileStation($this->credentials, $this->allowSelfSignedCertificate);

		return $this->fileStation;
	}

	/**
	 * Gets the Synology DownloadStation
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @return SynologyDownloadStation
	 */
	public function getDownloadStation() : SynologyDownloadStation
	{
		if ($this->downloadStation === null)
			$this->downloadStation = new SynologyDownloadStation();

		return $this->downloadStation;
	}

	/**
	 * Gets the Synology AudioStation
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @return SynologyAudioStation
	 */
	public function getAudioStation() : SynologyAudioStation
	{
		if ($this->audioStation === null)
			$this->audioStation = new SynologyAudioStation();

		return $this->audioStation;
	}

	/**
	 * Logs out of all the logged in sessions
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 */
	public function logout()
	{
		if ($this->audioStation !== null)
			$this->audioStation->logout();

		if ($this->downloadStation !== null)
			$this->downloadStation->logout();

		if ($this->fileStation !== null)
			$this->fileStation->logout();

		$this->audioStation = $this->downloadStation = $this->fileStation = null;
	}
}