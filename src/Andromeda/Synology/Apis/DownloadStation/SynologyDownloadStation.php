<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 28/09/2018
 * Time: 21:22
 */

namespace Andromeda\Synology\Apis\DownloadStation;

/**
 * Class SynologyDownloadStation
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Synology\Apis\DownloadStation
 */
class SynologyDownloadStation
{
	public function logout() : void
	{

	}
}