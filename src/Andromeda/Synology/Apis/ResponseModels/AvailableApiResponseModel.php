<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 17/12/2018
 * Time: 19:31
 */

namespace Andromeda\Synology\Apis\ResponseModels;

use Andromeda\Synology\IResponseModel;

/**
 * Class AvailableApiResponseModel
 *
 * @since 1.0.2
 * @author  MartijnW
 * @package Andromeda\Synology\Apis\ResponseModels
 */
class AvailableApiResponseModel implements IResponseModel
{
	/** @var string */
	public $path;

	/** @var int */
	public $minVersion;

	/** @var int */
	public $maxVersion;
}