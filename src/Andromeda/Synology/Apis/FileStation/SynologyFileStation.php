<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 28/09/2018
 * Time: 21:21
 */

namespace Andromeda\Synology\Apis\FileStation;

use Andromeda\Data\Curl\CurlRequest;
use Andromeda\Synology\Apis\FileStation\ResponseModels\DirectoriesResponseModel;
use Andromeda\Synology\Apis\FileStation\ResponseModels\InfoResponseModel;
use Andromeda\Synology\Apis\FileStation\ResponseModels\RenameResponseModel;
use Andromeda\Synology\Apis\FileStation\ResponseModels\UploadResponseModel;
use Andromeda\Synology\IResponseModel;
use Andromeda\Synology\SynologyApi;
use Andromeda\Synology\SynologyCredentials;
use CURLFile;
use Exception;

/**
 * Class SynologyFileStation
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Synology\Apis\FileStation
 */
class SynologyFileStation
{
	use SynologyApi
	{
		SynologyApi::logout as private _logout;
	}

	private const LOGIN_IDENTIFIER = 'FileStation';
	private const BASE_API_PATH = 'SYNO.FileStation';

	/**
	 * SynologyFileStation constructor.
	 *
	 * @since 1.0.2
	 * @param SynologyCredentials $credentials
	 * @param bool                $allowSelfSignedCertificate
	 * @throws Exception
	 */
	public function __construct(SynologyCredentials $credentials, bool $allowSelfSignedCertificate = false)
	{
		$this->allowSelfSignedCertificate = $allowSelfSignedCertificate;
		$this->credentials = $credentials;

		try
		{
			$this->sid = $this->login(self::LOGIN_IDENTIFIER);
		}
		catch (\Exception $ex)
		{
			throw $ex;
		}
	}

	/**
	 * Destructs the current object
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 */
	public function __destruct()
	{
		$this->logout();
	}

	public function checkPermission()
	{
		throw new \Exception('Not yet implemented!');
	}

	public function compress()
	{
		throw new \Exception('Not yet implemented!');
	}

	public function copy()
	{
		throw new \Exception('Not yet implemented!');
	}

	public function createDirectory()
	{
		throw new \Exception('Not yet implemented!');
	}

	public function delete()
	{
		throw new \Exception('Not yet implemented!');
	}

	/**
	 * Downloads a file from the DiskStation
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param DownloadRequestModel $request
	 * @return bool
	 */
	public function download(DownloadRequestModel $request) : bool
	{
		$parameters = $this->buildParameters($request);
		$response = $this->downloadFile($this->buildUrl('entry', self::BASE_API_PATH . '.Download', 'download', 2, $parameters) . "&_sid=$this->sid", $request->getDownloadTo());

		return $response;
	}

	public function extract()
	{
		throw new \Exception('Not yet implemented!');
	}

	public function getBackgroundTasks()
	{
		throw new \Exception('Not yet implemented!');
	}

	public function getDirectorySize()
	{
		throw new \Exception('Not yet implemented!');
	}

	public function getFavorites()
	{
		throw new \Exception('Not yet implemented!');
	}

	/**
	 * Gets the FileStation info
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @return IResponseModel|InfoResponseModel|null
	 */
	public function getInfo() : ?InfoResponseModel
	{
		$response = $this->performRequest($this->buildUrl('entry', self::BASE_API_PATH . '.Info', 'get', 2));

		return $this->decodeInto(InfoResponseModel::class, $this->getJsonData($response));
	}

	public function getMD5()
	{
		// todo: this task is async, maybe implement with callbacks?

		throw new \Exception('Not yet implemented!');
	}

	/**
	 * Gets a list of all directories
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param GetDirectoriesRequestModel|null $request
	 * @return IResponseModel|DirectoriesResponseModel|null
	 */
	public function getDirectories(?GetDirectoriesRequestModel $request = null) : ?DirectoriesResponseModel
	{
		$parameters = $this->buildParameters($request ?? new GetDirectoriesRequestModel());
		$response = $this->performRequest($this->buildUrl('entry', self::BASE_API_PATH . '.List', 'list_share', 2, $parameters));

		return $this->decodeInto(DirectoriesResponseModel::class, $this->getJsonData($response));
	}

	public function getThumbnail()
	{
		throw new \Exception('Not yet implemented!');
	}

	public function getVirtualFolder()
	{
		throw new \Exception('Not yet implemented!');
	}

	public function logout()
	{
		$this->_logout(self::LOGIN_IDENTIFIER);
	}

	public function move()
	{
		throw new \Exception('Not yet implemented!');
	}

	/**
	 * Renames a file
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param RenameRequestModel $request
	 * @return RenameResponseModel|null|IResponseModel
	 */
	public function rename(RenameRequestModel $request) : ?RenameResponseModel
	{
		$parameters = $this->buildParameters($request);
		$response = $this->performRequest($this->buildUrl('entry', self::BASE_API_PATH . '.Rename', 'rename', 2, $parameters));

		return $this->decodeInto(RenameResponseModel::class, $this->getJsonData($response));
	}

	public function search()
	{
		throw new \Exception('Not yet implemented!');
	}

	public function share()
	{
		throw new \Exception('Not yet implemented!');
	}

	/**
	 * Uploads a file
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param UploadRequestModel $request
	 * @return UploadResponseModel|null|IResponseModel
	 */
	public function upload(UploadRequestModel $request) : ?UploadResponseModel
	{
		if (!is_file($request->getUploadFile()))
			return null;

		//todo: check if directory exists, if 'createParents' is off

		$post_request = new CurlRequest();

		if ($this->allowSelfSignedCertificate)
			$post_request->setOption(CURLOPT_SSL_VERIFYPEER, false)->setOption(CURLOPT_SSL_VERIFYHOST, false);

		$post_request->setOption(CURLOPT_TIMEOUT, 300);
		$post_request->setOption(CURLINFO_HEADER_OUT, true);
		$name = basename($request->getUploadFile());

		$data = ['path' => $request->getTargetPath()];

		//todo: maybe make a nice function of this if else stuff?
		if ($request->getOverwrite() !== null)
			$data['overwrite'] = $request->getOverwrite() ? 'true' : 'false';

		if ($request->getAccessTime() !== null && $request->getAccessTime() > 0)
			$data['atime'] = $request->getAccessTime();

		if ($request->getCreationTime() !== null && $request->getCreationTime() > 0)
			$data['crtime'] = $request->getCreationTime();

		if ($request->getModifyTime() !== null && $request->getModifyTime() > 0)
			$data['mtime'] = $request->getModifyTime();

		if ($request->getCreateParents() !== null)
			$data['create_parents'] = $request->getCreateParents() ? 'true' : 'false';

		$data['file'] = new CURLFile($request->getUploadFile(), mime_content_type($request->getUploadFile()), $name);

		$url = $this->buildUrl('entry', self::BASE_API_PATH . '.Upload', 'upload', 2, '&_sid=' . $this->sid);
		$response = $post_request->postFormData($data, $url);

		return $this->decodeInto(UploadResponseModel::class, $this->getJsonData($response));
	}
}