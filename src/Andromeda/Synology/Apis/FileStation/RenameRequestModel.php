<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 08/10/2018
 * Time: 19:44
 */

namespace Andromeda\Synology\Apis\FileStation;

use Andromeda\Synology\IRequestModel;

/**
 * Class RenameRequestModel
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Synology\Apis\FileStation
 */
final class RenameRequestModel implements IRequestModel
{
	/**
	 * @var string
	 */
	protected $path;

	/**
	 * @var string
	 */
	protected $name;

	/**
	 * @var string
	 */
	protected $additional;

	/**
	 * @var int
	 */
	protected $search_taskid;

	/**
	 * RenameRequestModel constructor.
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param $path
	 * @param $name
	 */
	public function __construct($path, $name)
	{
		if (is_array($path) && is_array($name) && count($path) !== count($name))
			return;

		$this->setPath($path);
		$this->setName($name);
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return string
	 */
	public function getPath() : string
	{
		return $this->path;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param array|string $path
	 */
	public function setPath($path) : void
	{
		if (is_array($path))
			$this->path = json_encode($path);
		else if (is_string($path))
			$this->path = $path;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return string
	 */
	public function getName() : string
	{
		return $this->name;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param array|string $name
	 */
	public function setName($name) : void
	{
		if (is_array($name))
			$this->name = json_encode($name);
		else if (is_string($name))
			$this->name = $name;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return string|string
	 */
	public function getAdditional() : ?string
	{
		return $this->additional;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $additional
	 */
	public function setAdditional(string $additional) : void
	{
		$this->additional = $additional;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return int|null
	 */
	public function getSearchTaskid() : ?int
	{
		return $this->search_taskid;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param int $search_taskid
	 */
	public function setSearchTaskid(int $search_taskid) : void
	{
		$this->search_taskid = $search_taskid;
	}
}