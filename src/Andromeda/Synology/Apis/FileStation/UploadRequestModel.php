<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 05/10/2018
 * Time: 20:49
 */

namespace Andromeda\Synology\Apis\FileStation;

use Andromeda\Synology\IRequestModel;

/**
 * Class UploadRequestModel
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Synology\Apis\FileStation
 */
class UploadRequestModel implements IRequestModel
{
	/**
	 * @var string
	 */
	protected $targetPath;

	/**
	 * @var bool
	 */
	protected $createParents;

	/**
	 * @var bool|null
	 */
	protected $overwrite;

	/**
	 * @var int|null
	 */
	protected $modifyTime;

	/**
	 * @var int|null
	 */
	protected $creationTime;

	/**
	 * @var int|null
	 */
	protected $accessTime;

	/**
	 * @var string
	 */
	protected $uploadFile;

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return string
	 */
	public function getTargetPath() : string
	{
		return $this->targetPath;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $targetPath
	 */
	public function setTargetPath(string $targetPath) : void
	{
		$this->targetPath = $targetPath;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return bool|null
	 */
	public function getCreateParents() : ?bool
	{
		return $this->createParents;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param bool $createParents
	 */
	public function setCreateParents(bool $createParents) : void
	{
		$this->createParents = $createParents;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return bool|null
	 */
	public function getOverwrite() : ?bool
	{
		return $this->overwrite;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param bool|null $overwrite
	 */
	public function setOverwrite(?bool $overwrite) : void
	{
		$this->overwrite = $overwrite;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return int|null
	 */
	public function getModifyTime() : ?int
	{
		return $this->modifyTime;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param int $modifyTime
	 */
	public function setModifyTime(int $modifyTime) : void
	{
		$this->modifyTime = $modifyTime;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return null|int
	 */
	public function getCreationTime() : ?int
	{
		return $this->creationTime;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param int $creationTime
	 */
	public function setCreationTime(int $creationTime) : void
	{
		$this->creationTime = $creationTime;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return int|null
	 */
	public function getAccessTime() : ?int
	{
		return $this->accessTime;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param int $accessTime
	 */
	public function setAccessTime(int $accessTime) : void
	{
		$this->accessTime = $accessTime;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return string
	 */
	public function getUploadFile() : string
	{
		return $this->uploadFile;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $uploadFile
	 */
	public function setUploadFile(string $uploadFile) : void
	{
		$this->uploadFile = $uploadFile;
	}
}