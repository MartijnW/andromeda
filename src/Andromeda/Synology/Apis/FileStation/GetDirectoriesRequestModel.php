<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 30/09/2018
 * Time: 19:03
 */

namespace Andromeda\Synology\Apis\FileStation;

use Andromeda\Synology\IRequestModel;

/**
 * Class GetDirectoriesRequestModel
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Synology\Apis\FileStation
 */
final class GetDirectoriesRequestModel implements IRequestModel
{
	public const SORT_BY_NAME = 'name';
	public const SORT_BY_USER = 'user';
	public const SORT_BY_GROUP = 'group';
	public const SORT_BY_LAST_MODIFIED = 'mtime';
	public const SORT_BY_ACCESS_TIME = 'atime';
	public const SORT_BY_CHANGE_TIME = 'ctime';
	public const SORT_BY_CREATE_TIME = 'crtime';
	public const SORT_BY_POSIX = 'posix';

	public const SORT_DIRECTION_ASC = 'asc';
	public const SORT_DIRECTION_DESC = 'desc';

	protected $offset;

	protected $limit;

	/**
	 * @map sort_by
	 * @var string
	 */
	protected $sortBy;

	/**
	 * @map sort_direction
	 * @var string
	 */
	protected $sortDirection;

	/**
	 * @map onlywritable
	 * @var string
	 */
	protected $onlyWritable;

	protected $additional;

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return mixed
	 */
	public function getOffset()
	{
		return $this->offset;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param mixed $offset
	 */
	public function setOffset($offset) : void
	{
		$this->offset = $offset;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return mixed
	 */
	public function getLimit()
	{
		return $this->limit;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param mixed $limit
	 */
	public function setLimit($limit) : void
	{
		$this->limit = $limit;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return string
	 */
	public function getSortBy() : string
	{
		return $this->sortBy;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $sortBy
	 */
	public function setSortBy(string $sortBy) : void
	{
		//todo: check if sortBy is valid.
		$this->sortBy = $sortBy;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return string
	 */
	public function getSortDirection() : string
	{
		return $this->sortDirection;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $sortDirection
	 */
	public function setSortDirection(string $sortDirection) : void
	{
		//todo: check if sortDirection is valid.
		$this->sortDirection = $sortDirection;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return string
	 */
	public function getOnlyWritable() : string
	{
		return $this->onlyWritable;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $onlyWritable
	 */
	public function setOnlyWritable(string $onlyWritable) : void
	{
		$this->onlyWritable = $onlyWritable;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return mixed
	 */
	public function getAdditional()
	{
		return $this->additional;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param mixed $additional
	 */
	public function setAdditional($additional) : void
	{
		$this->additional = $additional;
	}
}