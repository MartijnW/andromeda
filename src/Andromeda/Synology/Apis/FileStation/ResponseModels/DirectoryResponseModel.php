<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 30/09/2018
 * Time: 18:45
 */

namespace Andromeda\Synology\Apis\FileStation\ResponseModels;

use Andromeda\Synology\IResponseModel;

/**
 * Class DirectoryResponseModel
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Synology\Apis\FileStation\ResponseModels
 */
class DirectoryResponseModel implements IResponseModel
{
	/**
	 * @map isdir
	 * @var bool
	 */
	public $isDirectory;

	/**
	 * @var string
	 */
	public $name;

	/**
	 * @var string
	 */
	public $path;
}