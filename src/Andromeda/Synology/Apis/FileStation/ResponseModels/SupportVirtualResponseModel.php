<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 30/09/2018
 * Time: 13:32
 */

namespace Andromeda\Synology\Apis\FileStation\ResponseModels;

use Andromeda\Synology\IResponseModel;

/**
 * Class SupportVirtualResponseModel
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Synology\Apis\FileStation\ResponseModels
 */
class SupportVirtualResponseModel implements IResponseModel
{
	/**
	 * @map enable_iso_mount
	 * @var bool
	 */
	public $enableIsoMount;

	/**
	 * @map enable_remote_mount
	 * @var bool
	 */
	public $enableRemoteMount;
}