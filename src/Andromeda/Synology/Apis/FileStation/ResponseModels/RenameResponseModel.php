<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 08/10/2018
 * Time: 19:45
 */

namespace Andromeda\Synology\Apis\FileStation\ResponseModels;

use Andromeda\Synology\IResponseModel;

/**
 * Class RenameResponseModel
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Synology\Apis\FileStation\ResponseModels
 */
class RenameResponseModel implements IResponseModel
{
	/**
	 * @var \Andromeda\Synology\Apis\FileStation\ResponseModels\DirectoryResponseModel[]
	 */
	public $files;
}