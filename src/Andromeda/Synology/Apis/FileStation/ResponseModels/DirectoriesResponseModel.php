<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 30/09/2018
 * Time: 18:40
 */

namespace Andromeda\Synology\Apis\FileStation\ResponseModels;

use Andromeda\Synology\IResponseModel;

/**
 * Class DirectoriesResponseModel
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Synology\Apis\FileStation\ResponseModels
 */
class DirectoriesResponseModel implements IResponseModel
{
	/**
	 * @var int
	 */
	public $total;

	/**
	 * @var int
	 */
	public $offset;

	/**
	 * @var \Andromeda\Synology\Apis\FileStation\ResponseModels\DirectoryResponseModel[]
	 */
	public $shares;
}