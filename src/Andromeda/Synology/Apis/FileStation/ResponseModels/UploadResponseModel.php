<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 06/10/2018
 * Time: 13:13
 */

namespace Andromeda\Synology\Apis\FileStation\ResponseModels;

use Andromeda\Synology\IResponseModel;

/**
 * Class UploadResponseModel
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Synology\Apis\FileStation\ResponseModels
 */
class UploadResponseModel implements IResponseModel
{
	/** @var bool */
	public $blSkip;

	/** @var string */
	public $file;

	/** @var int */
	public $pid;

	/** @var float */
	public $progress;
}