<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 30/09/2018
 * Time: 13:24
 */

namespace Andromeda\Synology\Apis\FileStation\ResponseModels;

use Andromeda\Synology\IResponseModel;

/**
 * Class InfoResponseModel
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Synology\Apis\FileStation\ResponseModels
 */
class InfoResponseModel implements IResponseModel
{
	/**
	 * @map enable_list_usergrp
	 * @var string
	 */
	public $enableListUsergrp;

	/**
	 * @var string
	 */
	public $hostname;

	/**
	 * @map is_manager
	 * @var bool
	 */
	public $isManager;

	/**
	 * @var array
	 */
	public $items;

	/**
	 * @map support_file_request
	 * @var bool
	 */
	public $supportFileRequest;

	/**
	 * @map support_sharing
	 * @var bool
	 */
	public $supportSharing;

	/**
	 * @map support_vfs
	 * @var bool
	 */
	public $supportVfs;

	/**
	 * @map support_virtual
	 * @var \Andromeda\Synology\Apis\FileStation\ResponseModels\SupportVirtualResponseModel
	 */
	public $supportVirtual;

	/**
	 * @map support_virtual_protocol
	 * @var array
	 */
	public $supportVirtualProtocol;

	/**
	 * @map system_codepage
	 * @var string
	 */
	public $systemCodepage;

	/**
	 * @var int
	 */
	public $uid;
}