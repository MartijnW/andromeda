<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 08/10/2018
 * Time: 21:52
 */

namespace Andromeda\Synology\Apis\FileStation;

use Andromeda\Synology\IRequestModel;

/**
 * Class DownloadRequestModel
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Synology\Apis\FileStation
 */
final class DownloadRequestModel implements IRequestModel
{
	public const MODE_OPEN = 'open';
	public const MODE_DOWNLOAD = 'download';

	/**
	 * @var string
	 */
	protected $path;

	/**
	 * @var string
	 */
	protected $mode = self::MODE_OPEN;

	/**
	 * @var string
	 */
	private $downloadTo;

	/**
	 * @var bool
	 */
	private $overwrite = false;

	/**
	 * DownloadRequestModel constructor.
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string|array $path
	 * @param string       $downloadTo
	 * @param bool         $overwrite
	 * @throws \Exception
	 */
	public function __construct($path, string $downloadTo, bool $overwrite = false)
	{
		$this->overwrite = $overwrite;
		$this->setPath($path);
		$this->setDownloadTo($downloadTo);
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return string
	 */
	public function getPath() : string
	{
		return $this->path;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param array|string $path
	 */
	public function setPath($path) : void
	{
		if (is_array($path))
			$this->path = json_encode($path);
		else if (is_string($path))
			$this->path = $path;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return string
	 */
	public function getMode() : string
	{
		return $this->mode;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $mode
	 */
	public function setMode(string $mode) : void
	{
		$mode = strtolower($mode);
		if ($mode !== self::MODE_DOWNLOAD || $mode !== self::MODE_OPEN)
			$mode = self::MODE_OPEN;

		$this->mode = $mode;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return string
	 */
	public function getDownloadTo() : string
	{
		return $this->downloadTo;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $downloadTo
	 * @throws \Exception
	 */
	public function setDownloadTo(string $downloadTo) : void
	{
		$dir = dirname($downloadTo);
		$name = basename($downloadTo);

		if (($dir = realpath($dir)) === false || !is_writable($dir))
			throw new \Exception('Download destination is not a valid path, or is not writable.');

		$downloadTo = "$dir/$name";

		if (file_exists($downloadTo) && $this->overwrite)
			unlink($downloadTo);
		else if (file_exists($downloadTo))
			throw new \Exception('File already exists!');

		$this->downloadTo = $downloadTo;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return bool
	 */
	public function getOverwrite() : bool
	{
		return $this->overwrite;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param bool $overwrite
	 */
	public function setOverwrite(bool $overwrite) : void
	{
		$this->overwrite = $overwrite;
	}
}