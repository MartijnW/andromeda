<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 28/09/2018
 * Time: 21:22
 */

namespace Andromeda\Synology\Apis\AudioStation;

/**
 * Class SynologyAudioStation
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Synology\Apis\AudioStation
 */
class SynologyAudioStation
{
	public function logout() : void
	{

	}
}