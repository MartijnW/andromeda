<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 28/09/2018
 * Time: 17:06
 */

namespace Andromeda\Synology;

/**
 * Class SynologyCredentials
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Synology
 */
final class SynologyCredentials
{
	public $host;

	public $port;

	public $secureHttp = false;

	public $username;

	public $password;

	public $allowSelfSignedCertificate = false;

	/**
	 * SynologyCredentials constructor.
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $host
	 * @param int    $port
	 * @param bool   $secureHttp
	 * @param string $username
	 * @param string $password
	 * @param bool   $allowSelfSignedCertificate
	 */
	public function __construct(string $host, int $port, bool $secureHttp, string $username, string $password, bool $allowSelfSignedCertificate = false)
	{
		$this->host = $host;
		$this->port = $port;
		$this->secureHttp = $secureHttp;
		$this->username = $username;
		$this->password = $password;
		$this->allowSelfSignedCertificate = $allowSelfSignedCertificate;
	}

	/**
	 * Gets the full host name
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @author MartijnW
	 * @return string
	 */
	public function getFullHostName() : string
	{
		$http = ($this->secureHttp ? 'https' : 'http');

		return "$http://$this->host:$this->port/";
	}
}