<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 30/09/2018
 * Time: 13:35
 */

namespace Andromeda\Synology;

/**
 * Interface IResponseModel
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Synology
 */
interface IResponseModel
{

}