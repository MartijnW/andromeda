<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 29/09/2018
 * Time: 15:58
 */

namespace Andromeda\Synology;

use Andromeda\Data\Curl\CurlRequest;
use Andromeda\Util\ReflectionUtil;
use Andromeda\Util\StringUtil;
use ReflectionClass;
use ReflectionProperty;

/**
 * Trait SynologyApi
 * Provides basic functionality for Synology APIs
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Synology
 */
trait SynologyApi
{
	/** @var SynologyCredentials Stores the connection information to the DiskStation */
	protected $credentials;

	/** @var bool Wheater or not to allow self signed certificates, or invalid certificates */
	protected $allowSelfSignedCertificate = false;

	/** @var string Contains the unique identifier needed for protected API calls */
	protected $sid = '';

	/**
	 * Gets a mapping
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $mapping
	 * @param array  $attributes
	 * @return array<string, string>
	 */
	private function getMapping(string $mapping, array $attributes) : array
	{
		$type = null;
		$map = null;

		foreach ($attributes as $name => $attribute)
		{
			if (isset($attribute['var']))
				$type = $attribute['var'];

			if (isset($attribute['map']) && $attribute['map'] === $mapping)
				$map = $attribute['map'];

			if ($name === $mapping || ($attribute['map'] ?? '') === $mapping)
				return [$name, $map, $type];
		}

		return [null, null, null];
	}

	/**
	 * Logs the given session in
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $session
	 * @return null|string
	 * @throws \Exception
	 */
	protected function login(string $session) : ?string
	{
		$response = $this->performRequest($this->buildUrl('auth', 'SYNO.API.Auth', 'login', 3) . "&account={$this->credentials->username}&passwd={$this->credentials->password}&session=$session&format=cookie");

		if ($response === null || ($json = json_decode($response, true)) === false)
			throw new \Exception('Could not login!');

		return $json['data']['sid'] ?? null;
	}

	/**
	 * Logs out the given session and resets the @see $sid
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $session
	 */
	protected function logout(string $session) : void
	{
		$this->performRequest($this->buildUrl('auth', 'SYNO.API.Auth', 'logout', 1) . "&session=$session");
		$this->sid = '';
	}

	/**
	 * Performs a request
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $url
	 * @return null|string
	 */
	protected function performRequest(string $url) : ?string
	{
		if (!StringUtil::isEmpty($this->sid))
			$url .= '&_sid=' . $this->sid;

		$request = new CurlRequest();

		if ($this->allowSelfSignedCertificate)
			$request->setOption(CURLOPT_SSL_VERIFYPEER, false)->setOption(CURLOPT_SSL_VERIFYHOST, false);

		return $request->get($url);
	}

	/**
	 * Decodes API data into the given ResponseModel
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string     $class
	 * @param array|null $data
	 * @return IResponseModel|null
	 */
	protected function decodeInto(string $class, ?array $data) : ?IResponseModel
	{
		if ($data === null || empty($data))
			return null;

		if (!class_exists($class) || !((new $class()) instanceof IResponseModel))
			return null;

		try
		{
			$instance = new $class();
			$reflectionClass = new ReflectionClass($class);

			$attributes = [];
			foreach ($reflectionClass->getProperties(ReflectionProperty::IS_PUBLIC) as $property)
				$attributes[$property->getName()] = ReflectionUtil::getPropertyAttributes($instance, $property->getName());

			foreach ($data as $key => $value)
			{
				[$propertyName, $mapping, $type] = $this->getMapping($key, $attributes);

				if ($key === $mapping)
					$key = $propertyName;

				$is_array = StringUtil::endsWith($type ?? '', '[]');

				if ($type !== null)
					$type = str_replace('[]', '', $type);

				if ($type !== null && class_exists($type) && ReflectionUtil::isInstanceOf($type, IResponseModel::class))
				{
					if ($is_array && is_array($value))
					{
						$instance->$key = [];

						foreach ($value as $val)
							$instance->$key[] = $this->decodeInto($type, $val);
					}
					else
						$instance->$key = $this->decodeInto($type, $value);
				}
				else
					$instance->$key = $value;
			}
		}
		catch (\ReflectionException $ex)
		{
			return null;
		}

		return $instance;
	}

	/**
	 * Gets the JSON data after a request
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $str
	 * @return null
	 */
	protected function getJsonData(string $str)
	{
		$json = json_decode($str, true);

		return $json === false ? null : $json['data'] ?? null;
	}

	/**
	 * Builds a valid request URL from the given parameters
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $file
	 * @param string $api
	 * @param string $method
	 * @param int    $version
	 * @param string $optional
	 * @return string
	 */
	protected function buildUrl(string $file, string $api, string $method, int $version, string $optional = '')
	{
		$file = urlencode($file);
		$api = urlencode($api);
		$method = urlencode($method);

		return $this->credentials->getFullHostName() . "webapi/$file.cgi?api=$api&version=$version&method=$method$optional";
	}

	/**
	 * Builds the query parameters from a request model
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param IRequestModel $model
	 * @return string
	 */
	protected function buildParameters(IRequestModel $model) : string
	{
		$build_parameters = '';

		try
		{
			$ref_class = new ReflectionClass($model);

			foreach ($ref_class->getProperties(ReflectionProperty::IS_PROTECTED) as $property)
			{
				$property->setAccessible(true);
				$parameter_value = $property->getValue($model);
				if ($parameter_value === null)
					continue;

				if ($parameter_value instanceof IRequestModel)
					$parameter_value = $this->buildParameters($parameter_value);
				else
					$parameter_value = urlencode((string)$parameter_value);

				$attributes = ReflectionUtil::getPropertyAttributes($model, $property->getName());

				if (isset($attributes['ignore']))
					continue;

				$parameter_name = $attributes['map'] ?? $property->getName();

				$build_parameters .= "&$parameter_name=$parameter_value";
			}
		}
		catch (\ReflectionException $ex)
		{
			return '';
		}

		return $build_parameters;
	}

	/**
	 * Downloads a file to the given location
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $url
	 * @param string $to
	 * @return bool
	 */
	protected function downloadFile(string $url, string $to) : bool
	{
		$context = null;
		if ($this->allowSelfSignedCertificate)
			$context = stream_context_create(['ssl' => ['verify_peer' => false, 'verify_peer_name' => false]]);

		return file_put_contents($to, fopen($url, 'r', false, $context)) > 0;
	}
}