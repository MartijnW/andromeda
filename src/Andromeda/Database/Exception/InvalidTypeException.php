<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 01/01/2019
 * Time: 23:44
 */

namespace Andromeda\Database\Exception;

use Exception;

/**
 * Class InvalidTypeException
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Database\Exception
 */
class InvalidTypeException extends Exception
{
	/**
	 * InvalidTypeException constructor.
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $type
	 * @param string $expected_type
	 */
	public function __construct(string $type, string $expected_type)
	{
		parent::__construct("`$type` must be of the type `$expected_type`");
	}
}