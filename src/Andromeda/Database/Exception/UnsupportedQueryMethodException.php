<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 01/01/2019
 * Time: 23:36
 */

namespace Andromeda\Database\Exception;

use Exception;

/**
 * Class UnsupportedQueryMethodException
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Database\Exception
 */
class UnsupportedQueryMethodException extends Exception
{
	/**
	 * UnsupportedQueryMethodException constructor.
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $method
	 * @param string $driver
	 */
	public function __construct(string $method, string $driver)
	{
		parent::__construct("`$method` is not supported by $driver");
	}
}