<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 01/01/2019
 * Time: 23:33
 */

namespace Andromeda\Database\Exception;

use Exception;

/**
 * Class InvalidFileException
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Database\Exception
 */
class InvalidFileException extends Exception
{
	/**
	 * InvalidFileException constructor.
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $file
	 */
	public function __construct(string $file)
	{
		parent::__construct("The given database file `$file` does not exist, or is not readable");
	}
}