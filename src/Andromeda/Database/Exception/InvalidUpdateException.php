<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 01/01/2019
 * Time: 23:41
 */

namespace Andromeda\Database\Exception;

use Exception;

/**
 * Class InvalidUpdateException
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Database\Exception
 */
class InvalidUpdateException extends Exception
{
	/**
	 * InvalidUpdateException constructor.
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 */
	public function __construct()
	{
		parent::__construct('The amount of fields to update is not equal to the amount of given values');
	}
}