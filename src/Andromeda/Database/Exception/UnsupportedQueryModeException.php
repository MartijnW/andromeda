<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 01/01/2019
 * Time: 23:24
 */

namespace Andromeda\Database\Exception;

use Exception;

/**
 * Class UnsupportedQueryModeException
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Database\Exception
 */
class UnsupportedQueryModeException extends Exception
{
	/**
	 * UnsupportedQueryModeException constructor.
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $mode
	 */
	public function __construct(string $mode)
	{
		parent::__construct("Unsupported query mode: `$mode`");
	}
}