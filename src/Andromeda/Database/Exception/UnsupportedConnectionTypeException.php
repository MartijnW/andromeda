<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 01/01/2019
 * Time: 23:28
 */

namespace Andromeda\Database\Exception;

use Exception;

/**
 * Class UnsupportedConnectionTypeException
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Database\Exception
 */
class UnsupportedConnectionTypeException extends Exception
{
	/**
	 * UnsupportedConnectionTypeException constructor.
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $connection_type
	 */
	public function __construct(string $connection_type)
	{
		parent::__construct("The connection type `$connection_type` is not supported");
	}
}