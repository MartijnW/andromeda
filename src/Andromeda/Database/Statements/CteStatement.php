<?php
declare(strict_types=1);

namespace Andromeda\Database\Statements;

use Andromeda\Database\DatabaseManager;

/**
 * Class CteStatement
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Database\Statements
 */
class CteStatement
{
	/** @var string */
	private $name;

	/** @var DatabaseManager */
	private $query;

	/**
	 * CteStatement constructor.
	 *
	 * @since  1.0.2
	 * @author Martijn
	 * @param string          $name
	 * @param DatabaseManager $query
	 */
	public function __construct(string $name, DatabaseManager $query)
	{
		$this->name = $name;
		$this->query = $query;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return string
	 */
	public function getName() : string
	{
		return $this->name;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $name
	 */
	public function setName(string $name) : void
	{
		$this->name = $name;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return DatabaseManager
	 */
	public function getQuery() : DatabaseManager
	{
		return $this->query;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param DatabaseManager $query
	 */
	public function setQuery(DatabaseManager $query) : void
	{
		$this->query = $query;
	}
}