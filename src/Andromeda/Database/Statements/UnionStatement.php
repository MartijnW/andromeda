<?php
declare(strict_types=1);

namespace Andromeda\Database\Statements;

use Andromeda\Database\DatabaseManager;

/**
 * Class UnionStatement
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Database\Statements
 */
class UnionStatement
{
	public const MODE_SINGLE = 'single';
	public const MODE_ALL = 'all';

	/** @var string */
	private $mode;

	/** @var DatabaseManager */
	private $query;

	/**
	 * UnionStatement constructor.
	 *
	 * @since  1.0.2
	 * @author Martijn
	 * @param string          $mode
	 * @param DatabaseManager $query
	 */
	public function __construct(string $mode, DatabaseManager $query)
	{
		$this->mode = $mode;
		$this->query = $query;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return string
	 */
	public function getMode() : string
	{
		return $this->mode;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $mode
	 */
	public function setMode(string $mode) : void
	{
		$this->mode = $mode;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return DatabaseManager
	 */
	public function getQuery() : DatabaseManager
	{
		return $this->query;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param DatabaseManager $query
	 */
	public function setQuery(DatabaseManager $query) : void
	{
		$this->query = $query;
	}
}