<?php
declare(strict_types=1);

namespace Andromeda\Database\Statements;

/**
 * Class WhereStatement
 *
 * @author  MartijnW
 * @package Andromeda\Database\Statements
 */
class WhereStatement
{
	public const MODE_AND = 'AND';
	public const MODE_OR = 'OR';

	/** @var string|null */
	private $field;

	/** @var string|null */
	private $operator;

	/** @var mixed */
	private $value;

	/** @var string */
	private $mode;

	/** @var string|null */
	private $query = null;

	/**
	 * WhereStatement constructor.
	 *
	 * @param string      $field
	 * @param string      $operator
	 * @param mixed       $value
	 * @param string      $mode
	 * @param string|null $query
	 */
	public function __construct(?string $field, ?string $operator, $value, string $mode, ?string $query = null)
	{
		$this->field = $field;
		$this->operator = $operator;
		$this->value = $value;
		$this->mode = $mode;
		$this->query = $query;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return string
	 */
	public function getField() : string
	{
		return $this->field;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $field
	 */
	public function setField(string $field) : void
	{
		$this->field = $field;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return string
	 */
	public function getOperator() : string
	{
		return $this->operator;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $operator
	 */
	public function setOperator(string $operator) : void
	{
		$this->operator = $operator;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return mixed
	 */
	public function getValue()
	{
		return $this->value;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param mixed $value
	 */
	public function setValue($value) : void
	{
		$this->value = $value;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @return string
	 */
	public function getMode() : string
	{
		return $this->mode;
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $mode
	 */
	public function setMode(string $mode) : void
	{
		$this->mode = $mode;
	}

	/**
	 * @since 1.0.2
	 * @author MartijnW
	 * @return string|null
	 */
	public function getQuery() : ?string
	{
		return $this->query;
	}

	/**
	 * @since 1.0.2
	 * @author MartijnW
	 * @param string|null $query
	 */
	public function setQuery(?string $query) : void
	{
		$this->query = $query;
	}
}