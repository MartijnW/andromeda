<?php
declare(strict_types=1);

namespace Andromeda\Database\Statements;

/**
 * Class JoinStatement
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Database\Statements
 */
class JoinStatement
{
	/** @var string */
	private $type;

	/** @var string */
	private $table;

	/** @var string */
	private $tableAs;

	/** @var string */
	private $joinOn;

	/**
	 * JoinStatement constructor.
	 *
	 * @since 1.0.2
	 * @param $type
	 * @param $table
	 * @param $tableAs
	 * @param $join_on
	 */
	public function __construct(string $type, string $table, string $tableAs, string $join_on)
	{
		$this->type = $type;
		$this->table = $table;
		$this->tableAs = $tableAs;
		$this->joinOn = $join_on;
	}

	/**
	 * @author MartijnW
	 * @since  1.0.2
	 * @return string
	 */
	public function getType() : string
	{
		return $this->type;
	}

	/**
	 * @author MartijnW
	 * @since  1.0.2
	 * @param string $type
	 */
	public function setType(string $type) : void
	{
		$this->type = $type;
	}

	/**
	 * @author MartijnW
	 * @since  1.0.2
	 * @return string
	 */
	public function getTable() : string
	{
		return $this->table;
	}

	/**
	 * @author MartijnW
	 * @since  1.0.2
	 * @param string $table
	 */
	public function setTable(string $table) : void
	{
		$this->table = $table;
	}

	/**
	 * @author MartijnW
	 * @since  1.0.2
	 * @return string
	 */
	public function getTableAs() : string
	{
		return $this->tableAs;
	}

	/**
	 * @author MartijnW
	 * @since  1.0.2
	 * @param string $tableAs
	 */
	public function setTableAs(string $tableAs) : void
	{
		$this->tableAs = $tableAs;
	}

	/**
	 * @author MartijnW
	 * @since  1.0.2
	 * @return string
	 */
	public function getJoinOn() : string
	{
		return $this->joinOn;
	}

	/**
	 * @author Martijn
	 * @since  1.0.2
	 * @param string $joinOn
	 */
	public function setJoinOn(string $joinOn) : void
	{
		$this->joinOn = $joinOn;
	}
}