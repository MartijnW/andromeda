<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 01/12/2018
 * Time: 20:29
 */

namespace Andromeda\Database\MySql;

use Andromeda\Database\DatabaseManager;
use Andromeda\Database\Exception\InvalidUpdateException;
use Andromeda\Database\Statements\CteStatement;
use Exception;
use PDO;
use PDOException;

/**
 * Class MySqlManager
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Database\MySql
 */
abstract class MySqlManager extends DatabaseManager
{
	/** @var MySqlCredentials */
	private $credentials;

	/**
	 * MySqlManager constructor.
	 *
	 * @since 1.0.2
	 * @param MySqlCredentials $credentials    Credentials to connect to the database with
	 * @param bool             $throw_on_error If true, connection errors will be thrown
	 */
	public function __construct(MySqlCredentials $credentials, bool $throw_on_error)
	{
		$this->credentials = $credentials;

		parent::__construct($throw_on_error);
	}

	/**
	 * Will be called when a connection is needed
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 */
	protected function connect() : void
	{
		try
		{
			self::$connection = new PDO('mysql:host=' . $this->credentials->getHost() . ';dbname=' . $this->credentials->getDatabaseName() . ';charset=utf8;', $this->credentials->getUser(), $this->credentials->getPassword());

			$this->setDefaultConnectionAttributes();
		}
		catch (PDOException $ex)
		{
			if ($this->throwOnError)
				throw $ex;

			die('Could not create the connection to the database');
		}
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string      $fields   Fields to select
	 * @param string      $table    Table to select from
	 * @param string|null $table_as Alias for the table
	 * @return string
	 */
	protected function buildSelectStatement(string $fields, ?string $table, ?string $table_as) : string
	{
		$table_select = $table !== null ? "FROM `$table`" : '';

		if ($table_as !== null)
			$table_select .= " AS $table_as";

		return "SELECT $fields $table_select";
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $table  Table to insert into
	 * @param array  $fields Fields to insert
	 * @param array  ...$values Values to insert
	 * @return string
	 */
	protected function buildInsertStatement(string $table, array $fields, array ...$values) : string
	{
		$fields = implode(', ', $fields);

		$values_data = [];
		foreach ($values as $value)
			$values_data[] = '(' . implode(', ', $value) . ')';

		$values_data = implode(', ', $values_data);

		/** @noinspection SqlDialectInspection */
		/** @noinspection SqlResolve */
		return "INSERT INTO `$table` ($fields) VALUES $values_data";
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $table  Table to update
	 * @param array  $fields Fields to update
	 * @param array  $values Values to update the fields with
	 * @return string
	 * @throws InvalidUpdateException
	 */
	protected function buildUpdateStatement(string $table, array $fields, array $values) : string
	{
		if (count($fields) !== count($values))
			throw new InvalidUpdateException();

		$set_parts = [];
		$field_count = count($fields);
		for ($i = 0; $i < $field_count; $i++)
			$set_parts[] = "{$fields[$i]} = {$values[$i]}";

		$set = implode(', ', $set_parts);

		/** @noinspection SqlDialectInspection */
		return "UPDATE `$table` SET $set";
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $table Table to delete from
	 * @return string
	 */
	protected function buildDeleteStatement(string $table) : string
	{
		/** @noinspection SqlDialectInspection */
		return "DELETE FROM `$table`";
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param int|null $limit  The limit of the rows
	 * @param int|null $offset The offset of the rows
	 * @return string
	 */
	protected function buildLimitAndOffsetStatement(?int $limit, ?int $offset) : string
	{
		if ($limit < 1 && $offset < 1)
			return '';

		if ($limit > 0 && $offset > 0)
			return "LIMIT $limit OFFSET $offset";

		return "LIMIT $limit";
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param array $order_by Fields to order by
	 * @return null|array<string, string>
	 */
	protected function buildOrderByStatement(array $order_by) : string
	{
		$order_parts = [];

		foreach ($order_by as $field => $direction)
			$order_parts[] = "$field $direction ";

		return 'ORDER BY ' . implode(', ', $order_parts);
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string ...$fields Fields to group by
	 * @return string
	 */
	protected function buildGroupByStatement(string ...$fields) : string
	{
		return 'GROUP BY ' . implode(', ', $fields);
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $field    Where field
	 * @param string $operator Compare operator
	 * @param mixed  $value    Value to compare with
	 * @return string
	 */
	protected function buildWhereStatement(string $field, string $operator, $value) : string
	{
		return "$field $operator $value";
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string      $type     Type of the join
	 * @param string      $table    Table to join
	 * @param string|null $table_as Alias for the joined table
	 * @param string      $on       Join on statement
	 * @return string
	 */
	protected function buildJoinStatement(string $type, string $table, ?string $table_as, string $on) : string
	{
		$table = '`$table`';
		if ($table_as !== null)
			$table .= " AS $table_as";

		return "$type JOIN $table ON $on";
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param CteStatement[] $ctes
	 * @return string
	 * @throws Exception
	 */
	protected function buildCte(array $ctes) : string
	{
		$query_parts = [];

		foreach ($ctes as $cte)
			$query_parts[] = "`{$cte->getName()}` AS ({$cte->getQuery()->buildQuery(false)})";

		return 'WITH ' . implode(', ', $query_parts);
	}
}