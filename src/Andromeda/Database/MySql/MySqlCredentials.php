<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 12/09/2018
 * Time: 22:11
 */

namespace Andromeda\Database\MySql;

/**
 * Class MySqlCredentials
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Database
 */
final class MySqlCredentials
{
	/**
	 * @var string Contains the hostname (IP or domain)
	 */
	private $host;

	/**
	 * @var string Contains the database name
	 */
	private $databaseName;

	/**
	 * @var string Contains the database user
	 */
	private $user;

	/**
	 * @var string Contains the password for the given database user
	 */
	private $password;

	/**
	 * MySqlCredentials constructor.
	 *
	 * @since 1.0.2
	 * @param string $host         Host to connect to
	 * @param string $databaseName Database name
	 * @param string $user         User to login with
	 * @param string $password     Password for the given user
	 */
	public function __construct(string $host, string $databaseName, string $user, string $password)
	{
		$this->host = $host;
		$this->databaseName = $databaseName;
		$this->user = $user;
		$this->password = $password;
	}

	/**
	 * Gets the host
	 *
	 * @since 1.0.2
	 * @return string
	 */
	public function getHost() : string
	{
		return $this->host;
	}

	/**
	 * Sets the host
	 *
	 * @since 1.0.2
	 * @param string $host
	 */
	public function setHost(string $host) : void
	{
		$this->host = $host;
	}

	/**
	 * Gets the database name
	 *
	 * @since 1.0.2
	 * @return string
	 */
	public function getDatabaseName() : string
	{
		return $this->databaseName;
	}

	/**
	 * Sets the database name
	 *
	 * @since 1.0.2
	 * @param string $databaseName
	 */
	public function setDatabaseName(string $databaseName) : void
	{
		$this->databaseName = $databaseName;
	}

	/**
	 * Gets the user
	 *
	 * @since 1.0.2
	 * @return string
	 */
	public function getUser() : string
	{
		return $this->user;
	}

	/**
	 * Sets the user
	 *
	 * @since 1.0.2
	 * @param string $user
	 */
	public function setUser(string $user) : void
	{
		$this->user = $user;
	}

	/**
	 * Gets the password
	 *
	 * @since 1.0.2
	 * @return string
	 */
	public function getPassword() : string
	{
		return $this->password;
	}

	/**
	 * Sets the password
	 *
	 * @since 1.0.2
	 * @param string $password
	 */
	public function setPassword(string $password) : void
	{
		$this->password = $password;
	}
}