<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 13/11/2018
 * Time: 22:39
 */

namespace Andromeda\Database;

use Andromeda\Database\Exception\UnsupportedQueryModeException;
use Andromeda\Database\Statements\CteStatement;
use Andromeda\Database\Statements\JoinStatement;
use Andromeda\Database\Statements\UnionStatement;
use Andromeda\Database\Statements\WhereStatement;
use Andromeda\Util\RuntimeUtil;
use Andromeda\Util\StringUtil;
use Exception;
use PDO;
use PDOException;
use PDOStatement;

/**
 * Class DatabaseManager
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Database
 */
abstract class DatabaseManager
{
	use QueryBuilder;

	protected const MODE_SELECT = 'select';
	protected const MODE_SELECT_DISTINCT = 'select_distinct';
	protected const MODE_UPDATE = 'update';
	protected const MODE_DELETE = 'delete';
	protected const MODE_INSERT = 'insert';

	/** @var PDO */
	protected static $connection;

	/** @var string */
	protected static $calledClass;

	/** @var PDOException|null */
	protected static $errorInfo;

	/** @var string */
	protected $query;

	/** @var array<string, mixed> */
	protected $bindings = [];

	/** @var bool */
	protected $executed = false;

	/** @var PDOStatement */
	protected $preparedStatement;

	/** @var bool */
	protected $throwOnError = false;

	/** @var bool */
	protected $batchInsert = false;

	/** @var string */
	protected $columnSeperator = ', ';

	/** @var string */
	protected $unionKeyword = 'UNION';

	/** @var string */
	protected $unionAllKeyword = 'UNION ALL';

	/** @var null|int */
	protected $limit;

	/** @var null|int */
	protected $offset;

	/** @var array */
	protected $orderBy = [];

	/** @var string[] */
	protected $groupBy = [];

	/** @var WhereStatement[] */
	protected $where = [];

	/** @var JoinStatement[] */
	protected $joins = [];

	/** @var mixed[] */
	protected $insertValues = [];

	/** @var mixed[] */
	protected $updateValues = [];

	/** @var string */
	protected $whereStatement = 'WHERE';

	/** @var UnionStatement[] */
	protected $unions = [];

	/** @var CteStatement[] */
	protected $ctes = [];

	/** @var string[] */
	private $selectFields = [];

	/** @var string|array */
	private $table;

	/** @var string */
	private $mode;

	/**
	 * Gets the error info of the last query
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @return Exception|null
	 */
	public static function errorInfo() : ?PDOException
	{
		return self::$errorInfo;
	}

	/**
	 * Disposes the current connection
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 */
	public static function dispose() : void
	{
		self::$connection = null;
	}

	/**
	 * DatabaseManager constructor.
	 *
	 * @since 1.0.2
	 * @param bool $throw_on_error
	 */
	public function __construct(bool $throw_on_error)
	{
		$this->throwOnError = $throw_on_error;

		$called_class = get_called_class();

		// Check if there is already a connection, if not, create one
		if (self::$connection == null || self::$calledClass != $called_class)
		{
			self::$calledClass = get_called_class();

			$this->connect();
		}
	}

	/**
	 * Sets the mode of the query
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $mode Mode to set the query to
	 */
	protected function setMode(string $mode) : void
	{
		$this->mode = $mode;
	}

	/**
	 * Sets the default connection attributes
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 */
	protected function setDefaultConnectionAttributes() : void
	{
		self::$connection->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
		self::$connection->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);
		self::$connection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}

	/**
	 * Adds the given select fields
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string ...$fields
	 */
	protected function addSelectField(string ...$fields) : void
	{
		$this->selectFields = array_merge($this->selectFields, $fields);
	}

	/**
	 * Sets the table to query from
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string      $table Table to query
	 * @param string|null $as    Alias of the table
	 */
	protected function setTable(string $table, ?string $as = null) : void
	{
		$this->table = $as !== null ? ['table' => $table, 'as' => $as] : $table;
	}

	/**
	 * Builds the query
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param bool $close_query
	 * @return string
	 * @throws UnsupportedQueryModeException
	 */
	protected function buildQuery(bool $close_query = true) : string
	{
		$query_parts = [];

		$table = $this->table['table'] ?? $this->table;
		$table_as = $this->table['as'] ?? null;

		if (count($this->ctes) > 0)
		{
			$query_parts[] = $this->buildCte($this->ctes);

			foreach ($this->ctes as $cte)
				$this->bindings = array_merge($this->bindings, $cte->getQuery()->bindings);
		}

		switch ($this->mode)
		{
			case self::MODE_SELECT:
				$query_parts[] = $this->buildSelectStatement(implode($this->columnSeperator, $this->selectFields), $table, $table_as);

				foreach ($this->joins as $join)
					$query_parts[] = $this->buildJoinStatement($join->getType(), $join->getTable(), $join->getTableAs(), $join->getJoinOn());

				if (count($this->where) > 0)
					$query_parts[] = $this->getWhereStatement();

				foreach ($this->unions as $union)
				{
					if ($union->getMode() === UnionStatement::MODE_SINGLE)
						$mode = $this->unionKeyword;
					else if ($union->getMode() === UnionStatement::MODE_ALL)
						$mode = $this->unionAllKeyword;
					else
						continue;

					$query_parts[] = "$mode " . $union->getQuery()->buildQuery(false);

					$this->bindings = array_merge($this->bindings, $union->getQuery()->bindings);
				}

				if (!empty($this->groupBy))
					$query_parts[] = $this->buildGroupByStatement(...$this->groupBy);

				if (!empty($this->orderBy))
					$query_parts[] = $this->buildOrderByStatement($this->orderBy);

				$query_parts[] = $this->buildLimitAndOffsetStatement($this->limit, $this->offset);

				return $this->getQuery($query_parts, $close_query);

			case self::MODE_INSERT:
				$query_parts[] = $this->buildInsertStatement($table, $this->selectFields, ...$this->insertValues);
				return $this->getQuery($query_parts, $close_query);

			case self::MODE_UPDATE:
				$query_parts[] = $this->buildUpdateStatement($table, array_keys($this->updateValues), array_values($this->updateValues));

				if (count($this->where) > 0)
					$query_parts[] = $this->getWhereStatement();

				return $this->getQuery($query_parts, $close_query);

			case self::MODE_DELETE:
				$query_parts[] = $this->buildDeleteStatement($table);

				if (count($this->where) > 0)
					$query_parts[] = $this->getWhereStatement();

				return $this->getQuery($query_parts, $close_query);

			default:
				throw new UnsupportedQueryModeException($this->mode);
		}
	}

	/**
	 * Gets the where statement
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @return string
	 */
	private function getWhereStatement() : string
	{
		$where_parts = [];

		foreach ($this->where as $statement)
		{
			if ($statement->getField() !== null && !StringUtil::isEmpty($statement->getField()))
				$where_parts[] = ['mode' => $statement->getMode(), 'query' => $this->buildWhereStatement($statement->getField(), $statement->getOperator(), $statement->getValue())];
			else
				$where_parts[] = ['mode' => $statement->getMode(), 'query' => $statement->getQuery()];
		}

		$where_statement = $this->whereStatement . ' ';
		$i = 0;
		foreach ($where_parts as $part)
			$where_statement = ++$i === 1 ? $where_statement . "{$part['query']} " : $where_statement . "{$part['mode']} {$part['query']} ";

		return $where_statement;
	}

	/**
	 * Gets the query from query parts
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string[] $parts Parts to combine
	 * @param bool     $close_query
	 * @return string
	 */
	private function getQuery(array $parts, bool $close_query = true) : string
	{
		return rtrim(implode(' ', $parts)) . ($close_query ? ';' : '');
	}

	/**
	 * Manually escapes a string.
	 * Not recommended. Please use prepared statements where possible.
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $string
	 * @param int    $mode
	 * @return string
	 */
	public function escape(string $string, int $mode) : string
	{
		return self::$connection->quote($string, $mode);
	}

	#region Query methods

	#region Static members

	/**
	 * Begins a transaction
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @return bool
	 */
	public static function begin() : bool
	{
		RuntimeUtil::createInstance(get_called_class());

		return self::$connection->beginTransaction();
	}

	/**
	 * Commits the currently active transaction
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @return bool
	 */
	public static function commit() : bool
	{
		RuntimeUtil::createInstance(get_called_class());

		return self::$connection->commit();
	}

	/**
	 * Rolls the current transaction back
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @return bool
	 */
	public static function rollback() : bool
	{
		RuntimeUtil::createInstance(get_called_class());

		return self::$connection->rollBack();
	}

	/**
	 * Checks if the driver is in a transaction
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @return bool
	 */
	public static function inTransaction() : bool
	{
		RuntimeUtil::createInstance(get_called_class());

		return self::$connection->inTransaction();
	}

	/**
	 * Starts a select statement
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $field     First field
	 * @param string ...$fields Other fields
	 * @return static
	 */
	public static function select(string $field = '*', string ...$fields) : self
	{
		/** @var DatabaseManager $class */
		$class = RuntimeUtil::createInstance(get_called_class());

		$class->addSelectField($field, ...$fields);
		$class->setMode(self::MODE_SELECT);
		return $class;
	}

	/**
	 * Starts a UPDATE statement
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $table Table to update
	 * @return static
	 */
	public static function update(string $table) : self
	{
		/** @var DatabaseManager $class */
		$class = RuntimeUtil::createInstance(get_called_class());

		$class->setTable($table);
		$class->setMode(self::MODE_UPDATE);
		return $class;
	}

	/**
	 * Starts a DELETE FROM statement
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $table Table to delete from
	 * @return static
	 */
	public static function deleteFrom(string $table) : self
	{
		/** @var DatabaseManager $class */
		$class = RuntimeUtil::createInstance(get_called_class());

		$class->setTable($table);
		$class->setMode(self::MODE_DELETE);
		return $class;
	}

	/**
	 * Starts a SELECT DISTINCT statement
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $field          Fisrt field
	 * @param string ...$extraFields Other fields
	 * @return static
	 */
	public static function selectDistinct(string $field = '*', string ...$extraFields) : self
	{
		/** @var DatabaseManager $class */
		$class = RuntimeUtil::createInstance(get_called_class());

		$class->addSelectField($field, ...$extraFields);
		$class->setMode(self::MODE_SELECT_DISTINCT);
		return $class;
	}

	/**
	 * Starts a INSERT INTO statement
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $table     Table to insert in
	 * @param string ...$fields Fields to insert
	 * @return static
	 */
	public static function insertInto(string $table, string ...$fields) : self
	{
		/** @var DatabaseManager $class */
		$class = RuntimeUtil::createInstance(get_called_class());

		$class->addSelectField(...$fields);
		$class->setTable($table);
		$class->setMode(self::MODE_INSERT);
		return $class;
	}

	/**
	 * Manual query. Not recommended (only for use in exports or very complicated queries).
	 * Make sure you escape all the fields using escape()
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $query
	 * @return static
	 */
	public static function query(string $query) : self
	{
		/** @var DatabaseManager $class */
		$class = RuntimeUtil::createInstance(get_called_class());

		$class->query = $query;
		return $class;
	}

	#endregion

	/**
	 * Creates a FROM statement
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string      $table Table to query
	 * @param string|null $as    Alias of the table
	 * @return static
	 */
	public function from(string $table, ?string $as = null) : self
	{
		$this->setTable($table, $as);
		return $this;
	}

	/**
	 * Creates a WHERE statement.
	 * WARNING: for parameters use :param_name and use the bind() function! (eliminates SQL injection!)
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $field    Field to check
	 * @param string $operator Compare operator
	 * @param mixed  $value    Value to check with
	 * @return static
	 */
	public function where(string $field, string $operator, $value) : self
	{
		$this->where[] = new WhereStatement($field, $operator, $value, WhereStatement::MODE_AND);
		return $this;
	}

	/**
	 * Creates a raw WHERE statement.
	 * WARNING: for parameters use :param_name and use the bind() function! (eliminates SQL injection!)
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $where Raw SQL where statement
	 * @return static
	 */
	public function whereRaw(string $where) : self
	{
		$this->where[] = new WhereStatement(null, null, null, WhereStatement::MODE_AND, $where);
		return $this;
	}

	/**
	 * Creates a OR WHERE statement
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $field    Field to check
	 * @param string $operator Compare operator
	 * @param mixed  $value    Value to check with
	 * @return static
	 */
	public function orWhere(string $field, string $operator, $value) : self
	{
		$this->where[] = new WhereStatement($field, $operator, $value, WhereStatement::MODE_OR);
		return $this;
	}

	/**
	 * Adds a ORDER BY statement
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $field     Field to order by
	 * @param string $direction Direction to order by
	 * @return static
	 */
	public function orderBy(string $field, string $direction) : self
	{
		$this->orderBy[$field] = $direction;
		return $this;
	}

	/**
	 * Adds a GROUP BY statement
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $field Field to group by
	 * @return static
	 */
	public function groupBy(string $field) : self
	{
		$this->groupBy[] = $field;
		return $this;
	}

	/**
	 * Creates a LIMIT statement
	 * Limits the amount of results that is returned
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param int $limit The limit
	 * @return static
	 */
	public function limit(int $limit) : self
	{
		$this->limit = $limit;
		return $this;
	}

	/**
	 * Creates a OFFSET statement
	 * Sets the select start offset
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param int $offset The offset
	 * @return static
	 */
	public function offset(int $offset) : self
	{
		$this->offset = $offset;
		return $this;
	}

	/**
	 * Creates a VALUES statement (insert statements)
	 * Use bind() to bind the values
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param mixed ...$values Values to insert
	 * @return static
	 */
	public function values(...$values) : self
	{
		$this->insertValues[] = $values;
		return $this;
	}

	/**
	 * Creates a batch VALUES statement
	 * No bind() needed. Format: ['value1', 'value2']
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param array ...$data
	 * @return mixed
	 */
	public function valuesBatch(array ...$data)
	{
		$this->batchInsert = true;

		$question_marks = str_split(str_repeat('?', count($data[0] ?? 0)));
		$i = count($this->bindings);
		foreach ($data as $row)
		{
			$this->values(...$question_marks);

			foreach ($row as $value)
				$this->bind((string)++$i, $value);
		}

		return $this;
	}

	/**
	 * Creates a SET statement
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $field Field to set
	 * @param mixed  $value Value to set the field with
	 * @return static
	 */
	public function set(string $field, $value) : self
	{
		$this->updateValues[$field] = $value;
		return $this;
	}

	/**
	 * Creates a JOIN statement
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $table Table to join
	 * @param string $as    Alias of the table
	 * @param string $on    On statement
	 * @return static
	 */
	public function join(string $table, string $as, string $on) : self
	{
		$this->joins[] = new JoinStatement('', $table, $as, $on);
		return $this;
	}

	/**
	 * Creates a INNER JOIN statement
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $table Table to join
	 * @param string $as    Alias of the table
	 * @param string $on    On statement
	 * @return static
	 */
	public function innerJoin(string $table, string $as, string $on) : self
	{
		$this->joins[] = new JoinStatement('INNER', $table, $as, $on);
		return $this;
	}

	/**
	 * Creates a OUTER JOIN statement
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $table Table to join
	 * @param string $as    Alias of the table
	 * @param string $on    On statement
	 * @return static
	 */
	public function outerJoin(string $table, string $as, string $on) : self
	{
		$this->joins[] = new JoinStatement('FULL OUTER', $table, $as, $on);
		return $this;
	}

	/**
	 * Creates a LEFT JOIN statement
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $table Table to join
	 * @param string $as    Alias of the table
	 * @param string $on    On statement
	 * @return static
	 */
	public function leftJoin(string $table, string $as, string $on) : self
	{
		$this->joins[] = new JoinStatement('LEFT', $table, $as, $on);
		return $this;
	}

	/**
	 * Creates a RIGHT JOIN statement
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $table Table to join
	 * @param string $as    Alias of the table
	 * @param string $on    On statement
	 * @return static
	 */
	public function rightJoin(string $table, string $as, string $on) : self
	{
		$this->joins[] = new JoinStatement('RIGHT', $table, $as, $on);
		return $this;
	}

	/**
	 * Creates a FULL JOIN statement
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $table Table to join
	 * @param string $as    Alias of the table
	 * @param string $on    On statement
	 * @return static
	 */
	public function fullJoin(string $table, string $as, string $on) : self
	{
		$this->joins[] = new JoinStatement('FULL', $table, $as, $on);
		return $this;
	}

	/**
	 * Creates a UNION statement
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param DatabaseManager $query
	 * @return static
	 */
	public function union(DatabaseManager $query) : self
	{
		$this->unions[] = new UnionStatement(UnionStatement::MODE_SINGLE, $query);

		return $this;
	}

	/**
	 * Creates a UNION ALL statement
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param DatabaseManager $query
	 * @return static
	 */
	public function unionAll(DatabaseManager $query) : self
	{
		$this->unions[] = new UnionStatement(UnionStatement::MODE_ALL, $query);

		return $this;
	}

	/**
	 * Adds a CTE statement
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string          $name
	 * @param DatabaseManager $query
	 * @return static
	 */
	public function with(string $name, DatabaseManager $query) : self
	{
		$this->ctes[] = new CteStatement($name, $query);

		return $this;
	}

	/**
	 * Binds parameters to their values
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $name  Name of the parameter
	 * @param mixed  $value Value to bind the parameter with
	 * @return static
	 */
	public function bind(string $name, $value) : self
	{
		$this->bindings[$name] = $value;
		return $this;
	}

	/**
	 * Binds all parameters to their values
	 * Format: [':param_name' => $param_value]
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param array<string, mixed> $bindings The bindings
	 * @return static
	 */
	public function bindAll(array $bindings) : self
	{
		foreach ($bindings as $name => $value)
			$this->bind($name, $value);

		return $this;
	}

	/**
	 * Prepares the query for executing
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @return DatabaseManager
	 * @throws Exception
	 */
	public function prepare() : self
	{
		if (StringUtil::isEmpty($this->query))
			$this->query = $this->buildQuery();

		$prep = self::$connection->prepare($this->query);

		if (!empty($this->bindings))
		{
			foreach ($this->bindings as $key => $value)
			{
				$param = PDO::PARAM_STR;

				if (is_int($value) || is_bool($value))
					$param = PDO::PARAM_INT;
				else if (is_null($value))
					$param = PDO::PARAM_NULL;

				$prep->bindValue($key, $value, $param);
			}
		}

		$this->preparedStatement = $prep;
		return $this;
	}

	/**
	 * Executes a query, returns false on failure
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @return bool
	 */
	public function execute() : bool
	{
		try
		{
			if ($this->preparedStatement == null)
				$this->prepare();

			$result = $this->preparedStatement->execute();
			$this->executed = $result;
			return $result;
		}
		catch (PDOException $ex)
		{
			self::$errorInfo = $ex;
		}
		catch (Exception $ex)
		{
		}

		return false;
	}

	/**
	 * Fetches one row
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @return mixed
	 */
	public function fetch()
	{
		try
		{
			if ($this->preparedStatement == null)
				$this->prepare();

			if (!$this->executed)
				$this->execute();

			return $this->preparedStatement->fetch(PDO::FETCH_ASSOC);
		}
		catch (Exception $ex)
		{
			return false;
		}
	}

	/**
	 * Fetches one row into a DatabaseModel
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $class DatabaseModel to fetch into
	 * @return DatabaseModel|null
	 */
	public function fetchInto(string $class) : ?DatabaseModel
	{
		if (!class_exists($class) || !is_subclass_of($class, DatabaseModel::class))
			return null;

		$data = $this->fetch();
		if ($data === false)
			return null;

		/** @var DatabaseModel $class */
		$class = new $class();
		return $class->decode($data);
	}

	/**
	 * Fetches all rows
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @return array
	 */
	public function fetchAll() : array
	{
		try
		{
			if ($this->preparedStatement == null)
				$this->prepare();

			if (!$this->executed)
				$this->execute();

			return $this->preparedStatement->fetchAll(PDO::FETCH_ASSOC);
		}
		catch (Exception $ex)
		{
			return [];
		}
	}

	/**
	 * Fetches all row into a DatabaseModel
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $class DatabaseModel to fetch all into
	 * @return DatabaseModel[]
	 */
	public function fetchAllInto(string $class) : array
	{
		if (!class_exists($class) || !is_subclass_of($class, DatabaseModel::class))
			return null;

		$data = $this->fetchAll();
		$return_data = [];

		foreach ($data as $item)
		{
			/** @var DatabaseModel $class */
			$class = new $class();
			$return_data[] = $class->decode($item);
		}

		return $return_data;
	}

	#endregion

	/**
	 * Gets the row count of the query
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @return int
	 */
	public function rowCount() : int
	{
		try
		{
			if ($this->preparedStatement == null)
				$this->prepare();

			if (!$this->executed)
				$this->execute();

			return $this->preparedStatement->rowCount();
		}
		catch (Exception $ex)
		{
			return -1;
		}
	}

	/**
	 * Gets the last inserted id
	 * WARNING: use this BEFORE Commit() otherwise it will return 0
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string|null $name Name
	 * @return string
	 */
	public function lastInsertedId(?string $name = null) : string
	{
		return self::$connection->lastInsertId($name);
	}

	/**
	 * Resets the database manager to it's default settings.
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 */
	public function reset() : void
	{
		$this->query = null;
		$this->where = [];
		$this->selectFields = [];
		$this->table = '';
		$this->bindings = [];
		$this->mode = '';
		$this->limit = null;
		$this->offset = null;
		$this->orderBy = [];
		$this->groupBy = [];
		$this->joins = [];
		$this->insertValues = [];
		$this->updateValues = [];
		$this->batchInsert = false;
		self::$errorInfo = null;
	}

	/**
	 * Returns the Query
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @return string
	 */
	public function __toString() : string
	{
		try
		{
			return $this->buildQuery();
		}
		catch (Exception $ex)
		{
			return 'An error occurred while compiling the query: ' . $ex->getMessage();
		}
	}
}