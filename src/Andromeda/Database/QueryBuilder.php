<?php
declare(strict_types=1);

namespace Andromeda\Database;

use Andromeda\Database\Statements\CteStatement;

/**
 * Trait QueryBuilder
 *
 * @since 1.0.2
 * @author  MartijnW
 * @package Andromeda\Database
 */
trait QueryBuilder
{
	/**
	 * Will be called when a connection is needed
	 *
	 * @author MartijnW
	 */
	protected abstract function connect() : void;

	/**
	 * Builds a select statement conform the given values
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string      $fields   Fields to select
	 * @param string      $table    Table to select from
	 * @param string|null $table_as Alias of the table
	 * @return string
	 */
	protected abstract function buildSelectStatement(string $fields, ?string $table, ?string $table_as) : string;

	/**
	 * Builds a insert statement conform the given values
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $table     Table to insert into
	 * @param array  $fields    Fields to insert
	 * @param array  ...$values Values to insert
	 * @return string
	 */
	protected abstract function buildInsertStatement(string $table, array $fields, array ...$values) : string;

	/**
	 * Builds a update statement conform the given values
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $table  Table to update
	 * @param array  $fields Fields to update
	 * @param array  $values Values to update the fields with
	 * @return string
	 */
	protected abstract function buildUpdateStatement(string $table, array $fields, array $values) : string;

	/**
	 * Builds a delete statement conform the given values
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $table Table to delete from
	 * @return string
	 */
	protected abstract function buildDeleteStatement(string $table) : string;

	/**
	 * Builds a limit and offset statement conform the given values
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param int|null $limit  Limit of the result set
	 * @param int|null $offset Offset of the result set
	 * @return string
	 */
	protected abstract function buildLimitAndOffsetStatement(?int $limit, ?int $offset) : string;

	/**
	 * Builds a order by statement conform the given values
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param array<string, string> $order_by Order by values, format: ['column' => 'direction']
	 * @return null|array<string, string>
	 */
	protected abstract function buildOrderByStatement(array $order_by) : string;

	/**
	 * Builds a group by statement conform the given values
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string ...$fields Fields to group by
	 * @return string
	 */
	protected abstract function buildGroupByStatement(string ...$fields) : string;

	/**
	 * Builds a where statement conform the given values
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $field    Field to compare with
	 * @param string $operator Compare operator
	 * @param mixed  $value    Value to compare with
	 * @return string
	 */
	protected abstract function buildWhereStatement(string $field, string $operator, $value) : string;

	/**
	 * Builds a join statement conform the given values
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string      $type     Type of join (e.g. left or right)
	 * @param string      $table    Table to join with
	 * @param string|null $table_as Alias of the table
	 * @param string      $on       On statement
	 * @return string
	 */
	protected abstract function buildJoinStatement(string $type, string $table, ?string $table_as, string $on) : string;

	/**
	 * Builds a CTE statement conform the given values
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param CteStatement[] $ctes
	 * @return string
	 */
	protected abstract function buildCte(array $ctes) : string;
}