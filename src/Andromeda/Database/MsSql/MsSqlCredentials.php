<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 12/09/2018
 * Time: 22:11
 */

namespace Andromeda\Database\MsSql;

use Andromeda\Database\Exception\UnsupportedConnectionTypeException;
use Exception;

/**
 * Class MySqlCredentials
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Database
 */
final class MsSqlCredentials
{
	/** @var string Recommended for Linux or Windows connections */
	public const CONNECTION_TYPE_SQLSRV = 'sqlsrv';

	/** @var string Recommended for Linux connections when sqlsrv is not available. Make sure FreeTDS is installed. */
	public const CONNECTION_TYPE_DBLIB = 'dblib';

	/**
	 * @var string Contains the hostname (IP or domain)
	 */
	private $host;

	/**
	 * @var string Contains the database name
	 */
	private $databaseName;

	/**
	 * @var string Contains the database user
	 */
	private $user;

	/**
	 * @var string Contains the password for the given database user
	 */
	private $password;

	/**
	 * @var string Contains the connection method used for the connection to the server
	 */
	private $connectionType;

	/**
	 * MySqlCredentials constructor.
	 *
	 * @since 1.0.2
	 * @param string $host           Host to connect to
	 * @param string $databaseName   Database name
	 * @param string $user           User to login with
	 * @param string $password       Password for the given user
	 * @param string $connectionType Method to connect to the server, see CONNECTION_TYPE_x constants
	 * @throws Exception
	 */
	public function __construct(string $host, string $databaseName, string $user, string $password, string $connectionType = self::CONNECTION_TYPE_SQLSRV)
	{
		$this->setHost($host);
		$this->setDatabaseName($databaseName);
		$this->setUser($user);
		$this->setPassword($password);
		$this->setConnectionType($connectionType);
	}

	/**
	 * Gets the host
	 *
	 * @since 1.0.2
	 * @return string
	 */
	public function getHost() : string
	{
		return $this->host;
	}

	/**
	 * Sets the host
	 *
	 * @since 1.0.2
	 * @param string $host
	 */
	public function setHost(string $host) : void
	{
		$this->host = $host;
	}

	/**
	 * Gets the database name
	 *
	 * @since 1.0.2
	 * @return string
	 */
	public function getDatabaseName() : string
	{
		return $this->databaseName;
	}

	/**
	 * Sets the database name
	 *
	 * @since 1.0.2
	 * @param string $databaseName
	 */
	public function setDatabaseName(string $databaseName) : void
	{
		$this->databaseName = $databaseName;
	}

	/**
	 * Gets the user
	 *
	 * @since 1.0.2
	 * @return string
	 */
	public function getUser() : string
	{
		return $this->user;
	}

	/**
	 * Sets the user
	 *
	 * @since 1.0.2
	 * @param string $user
	 */
	public function setUser(string $user) : void
	{
		$this->user = $user;
	}

	/**
	 * Gets the password
	 *
	 * @since 1.0.2
	 * @return string
	 */
	public function getPassword() : string
	{
		return $this->password;
	}

	/**
	 * Sets the password
	 *
	 * @since 1.0.2
	 * @param string $password
	 */
	public function setPassword(string $password) : void
	{
		$this->password = $password;
	}

	/**
	 * Sets the connection type
	 *
	 * @since 1.0.2
	 * @param string $connection_type
	 * @throws UnsupportedConnectionTypeException
	 */
	public function setConnectionType(string $connection_type) : void
	{
		$connection_type = strtolower($connection_type);

		if ($connection_type !== self::CONNECTION_TYPE_SQLSRV && $connection_type !== self::CONNECTION_TYPE_DBLIB)
			throw new UnsupportedConnectionTypeException($connection_type);

		$this->connectionType = $connection_type;
	}

	/**
	 * Gets the connection type
	 *
	 * @since 1.0.2
	 * @return string
	 */
	public function getConnectionType() : string
	{
		return $this->connectionType;
	}
}