<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 11/11/2018
 * Time: 21:03
 */

namespace Andromeda\Database\MsSql;

use Andromeda\Database\DatabaseManager;
use Andromeda\Database\Exception\InvalidUpdateException;
use Andromeda\Database\Exception\UnsupportedConnectionTypeException;
use Andromeda\Database\Statements\CteStatement;
use Exception;
use PDO;
use PDOException;

/**
 * Class MsSqlManager
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Database\MsSql
 */
abstract class MsSqlManager extends DatabaseManager
{
	public const TOP_ROWS = 'ROWS';
	public const TOP_PERCENT = 'PERCENT';

	/** @var MsSqlCredentials */
	private $credentials;

	/** @var string */
	private $topMode = self::TOP_ROWS;

	/**
	 * MsSqlManager constructor.
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param MsSqlCredentials $credentials
	 * @param bool             $throw_on_error
	 */
	public function __construct(MsSqlCredentials $credentials, bool $throw_on_error = false)
	{
		$this->credentials = $credentials;

		parent::__construct($throw_on_error);
	}

	/**
	 * Will be called when a connection is needed
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @throws UnsupportedConnectionTypeException
	 * @throws PDOException
	 */
	protected function connect() : void
	{
		try
		{
			switch ($this->credentials->getConnectionType())
			{
				case MsSqlCredentials::CONNECTION_TYPE_SQLSRV:
					self::$connection = new PDO("sqlsrv:Server={$this->credentials->getHost()};Database={$this->credentials->getDatabaseName()}", $this->credentials->getUser(), $this->credentials->getPassword());
					break;

				case MsSqlCredentials::CONNECTION_TYPE_DBLIB:
					self::$connection = new PDO("dblib:version=8.0;charset=UTF-8;host={$this->credentials->getHost()};dbname={$this->credentials->getDatabaseName()};", $this->credentials->getUser(), $this->credentials->getPassword());
					break;

				default:
					throw new UnsupportedConnectionTypeException($this->credentials->getConnectionType());
			}

			$this->setDefaultConnectionAttributes();
		}
		catch (PDOException $ex)
		{
			if ($this->throwOnError)
				throw $ex;

			die('Could not create the connection to the database');
		}
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string      $fields
	 * @param string      $table
	 * @param string|null $table_as
	 * @return mixed
	 */
	protected function buildSelectStatement(string $fields, ?string $table, ?string $table_as) : string
	{
		$table_select = $table !== null ? "FROM [$table]" : '';

		if ($table_as !== null)
			$table_select .= " AS $table_as";

		if ($this->limit !== null && $this->limit > 0 && $this->offset < 1)
		{
			$limit = $this->limit;
			if ($this->topMode !== self::TOP_ROWS)
				$limit .= " {$this->topMode}";

			return "SELECT TOP {$limit} $fields $table_select";
		}

		return "SELECT $fields $table_select";
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string   $table
	 * @param string[] $fields
	 * @param array    ...$values
	 * @return string
	 */
	protected function buildInsertStatement(string $table, array $fields, array ...$values) : string
	{
		$fields = implode(', ', $fields);

		$values_data = [];
		foreach ($values as $value)
			$values_data[] = '(' . implode(', ', $value) . ')';

		$values_data = implode(', ', $values_data);

		/** @noinspection SqlDialectInspection */
		return "INSERT INTO [$table] ($fields) VALUES $values_data";
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string   $table
	 * @param string[] $fields
	 * @param mixed[]  $values
	 * @return string
	 * @throws InvalidUpdateException
	 */
	protected function buildUpdateStatement(string $table, array $fields, array $values) : string
	{
		if (count($fields) !== count($values))
			throw new InvalidUpdateException();

		$set_parts = [];
		$field_count = count($fields);
		for ($i = 0; $i < $field_count; $i++)
			$set_parts[] = "{$fields[$i]} = {$values[$i]}";

		$set = implode(', ', $set_parts);

		/** @noinspection SqlDialectInspection */
		return "UPDATE [$table] SET $set";
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $table
	 * @return string
	 */
	protected function buildDeleteStatement(string $table) : string
	{
		/** @noinspection SqlDialectInspection */
		return "DELETE FROM [$table]";
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param int|null $limit
	 * @param int|null $offset
	 * @return string
	 * @throws Exception
	 */
	protected function buildLimitAndOffsetStatement(?int $limit, ?int $offset) : string
	{
		if ($limit < 1 && $offset < 1)
			return '';

		if ($limit > 0 && $offset < 1)
			return '';

		if (empty($this->orderBy))
			throw new Exception('MS-SQL does not support limit and offset without a ORDER BY statement');

		return "OFFSET $offset ROWS FETCH NEXT $limit ROWS ONLY";
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param array $order_by
	 * @return string
	 */
	protected function buildOrderByStatement(array $order_by) : string
	{
		$order_parts = [];

		foreach ($order_by as $field => $direction)
			$order_parts[] = "$field $direction ";

		return 'ORDER BY ' . implode(', ', $order_parts);
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string ...$fields
	 * @return string
	 */
	protected function buildGroupByStatement(string ...$fields) : string
	{
		return 'GROUP BY ' . implode(', ', $fields);
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $field
	 * @param string $operator
	 * @param mixed  $value
	 * @return string
	 */
	protected function buildWhereStatement(string $field, string $operator, $value) : string
	{
		return "$field $operator $value";
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string      $type
	 * @param string      $table
	 * @param string|null $table_as
	 * @param string      $on
	 * @return string
	 */
	protected function buildJoinStatement(string $type, string $table, ?string $table_as, string $on) : string
	{
		$table = "[$table]";
		if ($table_as !== null)
			$table .= " AS $table_as";

		return "$type JOIN $table ON $on";
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param CteStatement[] $ctes
	 * @return string
	 * @throws Exception
	 */
	protected function buildCte(array $ctes) : string
	{
		$query_parts = [];

		foreach ($ctes as $cte)
			$query_parts[] = "[{$cte->getName()}] AS ({$cte->getQuery()->buildQuery(false)})";

		return 'WITH ' . implode(', ', $query_parts);
	}

	#region Overrides

	/**
	 * Creates a LIMIT statement
	 * Limits the amount of results that is returned
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param int    $limit The limit
	 * @param string $top_mode Mode of the limit (TOP x PERCENT or TOP x ROWS)
	 * @return static
	 */
	public function limit(int $limit, string $top_mode = self::TOP_ROWS) : DatabaseManager
	{
		if ($top_mode !== self::TOP_ROWS && $top_mode !== self::TOP_PERCENT)
			$top_mode = self::TOP_ROWS;

		$this->topMode = $top_mode;

		return parent::limit($limit);
	}

	#endregion
}