<?php
declare(strict_types=1);

namespace Andromeda\Database;

use Andromeda\Database\Exception\InvalidTypeException;

/**
 * Class DatabaseModel
 *
 * @since   1.0.1
 * @author  MartijnW
 * @package Andromeda\Database
 */
abstract class DatabaseModel
{
	private $mappings = [];
	private $subModels = [];

	/**
	 * Maps a database field to another property
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param $key
	 * @param $to
	 */
	protected function map($key, $to)
	{
		if (!key_exists($key, $this->mappings))
			$this->mappings[$key] = $to;
	}

	/**
	 * Adds a submodel for decoding
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param string $key
	 * @param string $class
	 * @throws InvalidTypeException
	 */
	protected function addSubModel(string $key, string $class)
	{
		if (key_exists($key, $this->subModels))
			return;

		if (!class_exists($class) || !is_subclass_of($class, self::class))
			throw new InvalidTypeException($class, DatabaseModel::class);

		$this->subModels[$key] = $class;
	}

	/**
	 * Decodes array data into a @see DatabaseModel
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param array $data
	 * @return DatabaseModel
	 */
	public function decode(array $data) : DatabaseModel
	{
		foreach ($data as $key => $value)
		{
			if (key_exists($key, $this->mappings))
				$key = $this->mappings[$key];

			if (property_exists($this, $key))
			{
				if (key_exists($key, $this->subModels))
				{
					/** @var DatabaseModel $model */
					$model = new $this->subModels[$key];
					$this->{$key} = $model->decode($value);
				}
				else
					$this->{$key} = $value;
			}
		}

		return $this;
	}
}