<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 01/12/2018
 * Time: 21:08
 */

namespace Andromeda\Database\SqLite;

use Andromeda\Database\DatabaseManager;
use Andromeda\Database\Exception\InvalidFileException;
use Andromeda\Database\Exception\InvalidUpdateException;
use Andromeda\Database\Exception\UnsupportedQueryMethodException;
use Andromeda\Database\Statements\CteStatement;
use Exception;
use PDO;
use PDOException;

/**
 * Class SqLiteManager
 *
 * @since   1.0.2
 * @author  MartijnW
 * @package Andromeda\Database\SqLite
 */
abstract class SqLiteManager extends DatabaseManager
{
	/** @var string */
	private $databasePath;

	/**
	 * SqLiteManager constructor.
	 *
	 * @since 1.0.2
	 * @param string $database_path
	 * @param bool   $throw_on_error
	 */
	public function __construct(string $database_path, bool $throw_on_error)
	{
		$this->databasePath = $database_path;

		parent::__construct($throw_on_error);
	}

	/**
	 * Will be called when a connection is needed
	 *
	 * @author MartijnW
	 * @throws InvalidFileException
	 * @throws PDOException
	 */
	protected function connect() : void
	{
		try
		{
			if (!is_file($this->databasePath) || !is_readable($this->databasePath))
			{
				if ($this->throwOnError)
					throw new InvalidFileException($this->databasePath);

				die((new InvalidFileException($this->databasePath))->getMessage());
			}

			self::$connection = new PDO("sqlite:{$this->databasePath}");

			$this->setDefaultConnectionAttributes();
		}
		catch (PDOException $ex)
		{
			if ($this->throwOnError)
				throw $ex;

			die('Could not create the database connection to the database');
		}
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string      $fields
	 * @param string      $table
	 * @param string|null $table_as
	 * @return string
	 */
	protected function buildSelectStatement(string $fields, ?string $table, ?string $table_as) : string
	{
		$table_select = $table !== null ? "FROM `$table`" : '';

		if ($table_as !== null)
			$table_select .= " AS $table_as";

		return "SELECT $fields $table_select";
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $table
	 * @param array  $fields
	 * @param array  ...$values
	 * @return string
	 */
	protected function buildInsertStatement(string $table, array $fields, array ...$values) : string
	{
		$fields = implode(', ', $fields);

		$values_data = [];
		foreach ($values as $value)
			$values_data[] = '(' . implode(', ', $value) . ')';

		$values_data = implode(', ', $values_data);

		/** @noinspection SqlDialectInspection */
		return "INSERT INTO `$table` ($fields) VALUES $values_data";
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $table
	 * @param array  $fields
	 * @param array  $values
	 * @return string
	 * @throws InvalidUpdateException
	 */
	protected function buildUpdateStatement(string $table, array $fields, array $values) : string
	{
		if (count($fields) !== count($values))
			throw new InvalidUpdateException();

		$set_parts = [];
		$field_count = count($fields);
		for ($i = 0; $i < $field_count; $i++)
			$set_parts[] = "{$fields[$i]} = {$values[$i]}";

		$set = implode(', ', $set_parts);

		/** @noinspection SqlDialectInspection */
		return "UPDATE `$table` SET $set";
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $table
	 * @return string
	 */
	protected function buildDeleteStatement(string $table) : string
	{
		/** @noinspection SqlDialectInspection */
		return "DELETE FROM `$table`";
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param int|null $limit
	 * @param int|null $offset
	 * @return string
	 */
	protected function buildLimitAndOffsetStatement(?int $limit, ?int $offset) : string
	{
		if ($limit < 1 && $offset < 1)
			return '';

		if ($limit > 0 && $offset > 0)
			return "LIMIT $limit OFFSET $offset";

		return "LIMIT $limit";
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param array $order_by
	 * @return null|array<string, string>
	 */
	protected function buildOrderByStatement(array $order_by) : string
	{
		$order_parts = [];

		foreach ($order_by as $field => $direction)
			$order_parts[] = "$field $direction ";

		return 'ORDER BY ' . implode(', ', $order_parts);
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string ...$fields
	 * @return string
	 */
	protected function buildGroupByStatement(string ...$fields) : string
	{
		return 'GROUP BY ' . implode(', ', $fields);
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $field
	 * @param string $operator
	 * @param mixed  $value
	 * @return string
	 */
	protected function buildWhereStatement(string $field, string $operator, $value) : string
	{
		return "$field $operator $value";
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string      $type
	 * @param string      $table
	 * @param string|null $table_as
	 * @param string      $on
	 * @return string
	 */
	protected function buildJoinStatement(string $type, string $table, ?string $table_as, string $on) : string
	{
		$table = '`$table`';
		if ($table_as !== null)
			$table .= " AS $table_as";

		return "$type JOIN $table ON $on";
	}

	/**
	 * @since  1.0.2
	 * @author MartijnW
	 * @param CteStatement[] $ctes
	 * @return string
	 * @throws Exception
	 */
	protected function buildCte(array $ctes) : string
	{
		$query_parts = [];

		foreach ($ctes as $cte)
			$query_parts[] = "`{$cte->getName()}` AS ({$cte->getQuery()->buildQuery(false)})";

		return 'WITH ' . implode(', ', $query_parts);
	}

	#region Unsupported method overrides

	/**
	 * RIGHT JOINS are not supported by SqLite
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $table
	 * @param string $as
	 * @param string $on
	 * @return static
	 * @throws UnsupportedQueryMethodException
	 */
	public function rightJoin(string $table, string $as, string $on) : DatabaseManager
	{
		throw new UnsupportedQueryMethodException('RIGHT JOIN', 'SqLite');
	}

	/**
	 * FULL JOINS are not supported byh SqLite
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $table
	 * @param string $as
	 * @param string $on
	 * @return static
	 * @throws UnsupportedQueryMethodException
	 */
	public function fullJoin(string $table, string $as, string $on) : DatabaseManager
	{
		throw new UnsupportedQueryMethodException('FULL JOIN', 'SqLite');
	}

	#endregion
}