<?php
declare(strict_types=1);

namespace Andromeda\Util;

use ReflectionClass;
use ReflectionException;
use ReflectionFunction;
use ReflectionMethod;
use ReflectionProperty;
use Reflector;

/**
 * Class ReflectionUtil
 *
 * @since   1.0.1
 * @author  MartijnW
 * @package Andromeda\Util
 */
class ReflectionUtil
{
	private static $attributeMappings = [
		'classes' => [],
		'functions' => []
	];

	private $example = [
		'classes' => [
			'className' => [
				'attributes' => [],
				'methods' => [
					'method1' => ['@var', '@biem'],
					'method2' => ['@a', '@b']
				],
				'properties' => [
					'prop1' => [],
					'prop2' => []
				]
			]
		],
		'functions' => [
			'func1' => []
		]
	];

	/**
	 * Gets all available attributes of a class
	 * Result array format:
	 * [
	 *    'attribute_name' => 'value',
	 *    'my_attribute_array' => [ 'value1', 'value2' ]
	 * ]
	 *
	 * @since 1.0.1
	 * @param string $class
	 * @return array<string, string|array>|null
	 */
	public static function getClassAttributes(string $class) : ?array
	{
		try
		{
			$class = new ReflectionClass($class);
		}
		catch (ReflectionException $ex)
		{
			return null;
		}

		$name = $class->getName();
		$mappings = self::$attributeMappings['classes'][$name] ?? null;
		if ($mappings === null)
			self::$attributeMappings['classes'][$name]['attributes'] = self::getAttributes($class);

		return self::$attributeMappings['classes'][$name]['attributes'] ?? null;
	}

	/**
	 * Gets all available attributes of a method
	 * Result array format:
	 * [
	 *    'attribute_name' => 'value',
	 *    'my_attribute_array' => [ 'value1', 'value2' ]
	 * ]
	 *
	 * @since 1.0.1
	 * @param mixed  $class
	 * @param string $method
	 * @return array<string, string|array>|null
	 */
	public static function getMethodAttributes($class, string $method) : ?array
	{
		try
		{
			$method = new ReflectionMethod($class, $method);
		}
		catch (ReflectionException $ex)
		{
			return null;
		}

		$name = $method->getName();
		$class_name = $method->getDeclaringClass()->getName();
		$mappings = self::$attributeMappings['classes'][$class_name]['methods'][$name] ?? null;
		if ($mappings === null)
			self::$attributeMappings['classes'][$class_name]['methods'][$name]['attributes'] = self::getAttributes($method);

		return self::$attributeMappings['classes'][$class_name]['methods'][$name]['attributes'];
	}

	/**
	 * Gets all available attributes of a property
	 * Result array format:
	 * [
	 *    'attribute_name' => 'value',
	 *    'my_attribute_array' => [ 'value1', 'value2' ]
	 * ]
	 *
	 * @since 1.0.1
	 * @param mixed  $class
	 * @param string $property
	 * @return array<string, string|array>|null
	 */
	public static function getPropertyAttributes($class, string $property) : ?array
	{
		try
		{
			$property = new ReflectionProperty($class, $property);
		}
		catch (ReflectionException $ex)
		{
			return null;
		}

		$name = $property->getName();
		$class_name = $property->getDeclaringClass()->getName();
		$mappings = self::$attributeMappings['classes'][$class_name]['properties'][$name] ?? null;
		if ($mappings === null)
			self::$attributeMappings['classes'][$class_name]['properties'][$name]['attributes'] = self::getAttributes($property);

		return self::$attributeMappings['classes'][$class_name]['properties'][$name]['attributes'];
	}

	/**
	 * Gets all available attributes of a function
	 * Result array format:
	 * [
	 *    'attribute_name' => 'value',
	 *    'my_attribute_array' => [ 'value1', 'value2' ]
	 * ]
	 *
	 * @since 1.0.1
	 * @param string $function
	 * @return array<string, string|array>|null
	 */
	public static function getFunctionAttributes(string $function) : ?array
	{
		try
		{
			$function = new ReflectionFunction($function);
		}
		catch (ReflectionException $ex)
		{
			return null;
		}

		$name = $function->getName();
		$mappings = self::$attributeMappings['functions'][$name] ?? null;
		if ($mappings === null)
			self::$attributeMappings['functions'][$name] = self::getAttributes($function);

		return self::$attributeMappings['functions'][$name];
	}

	/**
	 * Checks if the given class is an instance of another class
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $class
	 * @param string $of
	 * @return bool
	 */
	public static function isInstanceOf(string $class, string $of)
	{
		if (!class_exists($class) || !interface_exists($of))
			return false;

		return (new $class()) instanceof $of;
	}

	/**
	 * Gets the attributes of a class, method, property or function
	 * Returns null when there is no PhpDoc
	 *
	 * @since 1.0.1
	 * @param Reflector $reflect
	 * @return array|null
	 */
	private static final function getAttributes(Reflector $reflect) : ?array
	{
		if (!($reflect instanceof ReflectionClass) && !($reflect instanceof ReflectionMethod) && !($reflect instanceof ReflectionProperty) && !($reflect instanceof ReflectionFunction))
			return null;

		$doc = $reflect->getDocComment();
		if ($doc === false || StringUtil::isEmpty($doc))
			return null;

		$doc = str_replace(['/**', '*/'], '', $doc);
		$parts = explode(PHP_EOL, $doc);

		$data = [];

		foreach ($parts as $part)
		{
			$part = trim(StringUtil::replaceFirst('* ', '', $part));

			if (!StringUtil::startsWith($part, '@'))
				continue;

			$part = StringUtil::replaceFirst('@', '', $part);
			$split = explode(' ', $part, 2);

			$type = $split[0] ?? '';
			$value = ltrim($split[1] ?? '');

			if (StringUtil::isEmpty($type))
				continue;

			if (key_exists($type, $data))
			{
				if (!is_array($data[$type]))
					$data[$type] = [$data[$type], $value];
				else
					$data[$type][] = $value;
			}
			else
				$data[$type] = $value;
		}

		return $data;
	}
}