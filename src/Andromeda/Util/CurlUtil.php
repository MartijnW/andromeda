<?php
/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 21/03/2018
 * Time: 21:04
 */

namespace Andromeda\Util;

use Andromeda\Data\Curl\CurlRequest;

/**
 * Class CurlUtil
 * This class aims to make working with curl easier
 *
 * @author  MartijnW
 * @package Andromeda\Util
 * @since   1.0.0
 */
class CurlUtil extends CurlRequest
{
	public static function batch(CurlRequest ...$requests) : void
	{
		$mc = curl_multi_init();

		foreach ($requests as $request)
		{
			$request->prepare(null);
			curl_multi_add_handle($mc, $request->curl);
		}

		$running = null;
		do
			curl_multi_exec($mc, $running);
		while ($running);

		foreach ($requests as $request)
			curl_multi_remove_handle($mc, $request->curl);

		curl_multi_close($mc);

		foreach ($requests as $request)
			if ($request->callback !== null)
				call_user_func($request->callback, curl_multi_getcontent($request->curl));
	}
}