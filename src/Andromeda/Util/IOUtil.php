<?php
declare(strict_types=1);

namespace Andromeda\Util;

/**
 * Class IOUtil
 * @author  MartijnW
 * @package Andromeda\Util
 * @since   0.0.1
 */
class IOUtil
{
	/**
	 * Gets all the directories in the given path
	 * @param string $path
	 * @param bool   $realpath
	 * @author MartijnW
	 * @return \Generator
	 * @since  0.0.1
	 */
	public static function getDirectories(string $path, bool $realpath = false) : \Generator
	{
		if (!$realpath)
			$path = realpath($path);

		if ($path === false || !is_dir($path))
			return yield;

		yield from self::getItems($path, function (string $newPath) {
			return is_dir($newPath);
		});
	}

	/**
	 * Gets all the files in the given path
	 * @param string $path
	 * @param bool   $realpath
	 * @author MartijnW
	 * @return \Generator
	 */
	public static function getFiles(string $path, bool $realpath = false) : \Generator
	{
		if (!$realpath)
			$path = realpath($path);

		if ($path === false || !is_dir($path))
			return yield;

		yield from self::getItems($path, function (string $newPath) {
			return is_file($newPath);
		});
	}

	/**
	 * Gets all items in the given path with a custom callback function
	 * @param string   $path
	 * @param callable $func
	 * @author MartijnW
	 * @return \Generator
	 */
	private static function getItems(string $path, callable $func) : \Generator
	{
		$scan = scandir($path);
		if ($scan === false)
			return;

		foreach ($scan as $item)
		{
			if ($item == '.' || $item == '..')
				continue;

			$new_path = $path . DIRECTORY_SEPARATOR . $item;
			if (!$func($new_path))
				continue;

			yield $new_path;
		}
	}
}