<?php

namespace Andromeda\Util;

/**
 * Class RuntimeUtil
 *
 * @author  MartijnW
 * @package Andromeda\Util
 * @since   0.0.1
 */
class RuntimeUtil
{
	public const WINDOWS = 'windows';
	public const BSD = 'bsd';
	public const DARWIN = 'darwin';
	public const SOLARIS = 'solaris';
	public const LINUX = 'linux';

	/**
	 * Checks if the current OS is Windows
	 *
	 * @author MartijnW
	 * @return bool
	 * @since  0.0.1
	 */
	public static function isWindows() : bool
	{
		return strncasecmp(PHP_OS, 'WIN', 3) == 0;
	}

	/**
	 * Checks if the current OS is Linux
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @return bool
	 */
	public static function isLinux() : bool
	{
		return strtolower(PHP_OS_FAMILY) === self::LINUX;
	}

	/**
	 * Checks if the current OS is MacOS
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @return bool
	 */
	public static function isMacOS() : bool
	{
		return strtolower(PHP_OS_FAMILY) === self::DARWIN;
	}

	/**
	 * Checks if the current OS is BSD
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @return bool
	 */
	public static function isBSD() : bool
	{
		return strtolower(PHP_OS_FAMILY) === self::BSD;
	}

	/**
	 * Checks if the current OS is Solaris
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @return bool
	 */
	public static function isSolaris() : bool
	{
		return strtolower(PHP_OS_FAMILY) === self::SOLARIS;
	}

	/**
	 * Tries to execute the callable, and ignores it if it fails
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param callable $call
	 */
	public static function tryAndIgnore(callable $call) : void
	{
		try
		{
			$call();
		}
		catch (\Exception $ex)
		{
			// ignored
		}
	}

	/**
	 * Tries to create a new instance of the given class
	 *
	 * @since  1.0.2
	 * @author MartijnW
	 * @param string $class
	 * @param array  $params
	 * @return null|mixed
	 */
	public static function createInstance(string $class, ...$params)
	{
		if (!class_exists($class))
			return null;

		return new $class(...$params);
	}
}