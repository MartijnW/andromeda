<?php
declare(strict_types=1);

namespace Andromeda\Util;

/**
 * Class StringUtil
 *
 * @author  MartijnW
 * @package Andromeda\Util
 * @since   0.0.1
 */
class StringUtil
{
	/**
	 * Checks if the given string is null or empty
	 *
	 * @author MartijnW
	 * @param null|string $str
	 * @return bool
	 */
	public static function isEmpty(?string $str) : bool
	{
		return ($str === '0' || $str === '0.0' ? false : ($str === null || empty($str) || ctype_space($str)));
	}

	/**
	 * Checks if the haystack starts with the needle
	 *
	 * @author MartijnW
	 * @param string $haystack
	 * @param string $needle
	 * @return bool
	 */
	public static function startsWith(string $haystack, string $needle) : bool
	{
		return (substr($haystack, 0, strlen($needle)) === $needle);
	}

	/**
	 * Checks if the haystack ends with the needle
	 *
	 * @author MartijnW
	 * @param string $haystack
	 * @param string $needle
	 * @return bool
	 */
	public static function endsWith(string $haystack, string $needle) : bool
	{
		return $needle === '' || (substr($haystack, -strlen($needle)) === $needle);
	}

	/**
	 * Checks if the haystack contains the needle
	 *
	 * @author MartijnW
	 * @param string $haystack
	 * @param string $needle
	 * @return bool
	 */
	public static function contains(string $haystack, string $needle) : bool
	{
		return strpos($haystack, $needle) !== false;
	}

	/**
	 * Same as str_replace but only for the first occurence
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param string $search
	 * @param string $replace
	 * @param string $subject
	 * @return string
	 */
	public static function replaceFirst(string $search, string $replace, string $subject) : string
	{
		$pos = strpos($subject, $search);
		if ($pos === false)
			$pos = 0;

		return substr_replace($subject, $replace, $pos, strlen($search));
	}
}