<?php
declare(strict_types=1);

/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 03/01/2019
 * Time: 18:21
 */

namespace Andromeda\Util;

/**
 * List of most used hashing algorithms
 *
 * @since   1.0.3
 * @author  MartijnW
 * @package Andromeda\Util
 */
class HashAlgorithm
{
	#region CRC algorithms

	public const CRC32 = 'crc32';
	public const CRC32B = 'crc32b';

	#endregion

	#region FNV algorithms

	public const FNV132 = 'fnv132';
	public const FNV1A32 = 'fnv1a32';
	public const FNV164 = 'fnv164';
	public const FNV1A64 = 'fnv1a64';

	#endregion

	#region GOST algorithms

	public const GOST = 'gost';
	public const GOST_CRYPTO = 'gost-crypto';

	#endregion

	#region HAVAL algorithms

	public const HAVAL128_3 = 'haval128,3';
	public const HAVAL160_3 = 'haval160,3';
	public const HAVAL192_3 = 'haval192,3';
	public const HAVAL224_3 = 'haval224,3';
	public const HAVAL256_3 = 'haval256,3';
	public const HAVAL128_4 = 'haval128,4';
	public const HAVAL160_4 = 'haval160,4';
	public const HAVAL192_4 = 'haval192,4';
	public const HAVAL224_4 = 'haval224,4';
	public const HAVAL256_4 = 'haval256,4';
	public const HAVAL128_5 = 'haval128,5';
	public const HAVAL160_5 = 'haval160,5';
	public const HAVAL192_5 = 'haval192,5';
	public const HAVAL224_5 = 'haval224,5';
	public const HAVAL256_5 = 'haval256,5';

	#endregion

	#region MD algorithms

	public const MD2 = 'md2';
	public const MD4 = 'md4';
	public const MD5 = 'md5';

	#endregion

	#region RIPEMD algorithms

	public const RIPEMD_128 = 'ripemd_128';
	public const RIPEMD_160 = 'ripemd_160';
	public const RIPEMD_256 = 'ripemd_256';
	public const RIPEMD_320 = 'ripemd320';

	#endregion

	#region SHA algorithms

	public const SHA1 = 'sha1';
	public const SHA224 = 'sha224';
	public const SHA256 = 'sha256';
	public const SHA384 = 'sha384';
	public const SHA512 = 'sha512';
	public const SHA512_224 = 'sha512/224';
	public const SHA512_256 = 'sha512/256';

	public const SHA3_224 = 'sha3-224';
	public const SHA3_256 = 'sha3-256';
	public const SHA3_384 = 'sha3-384';
	public const SHA3_512 = 'sha3-512';

	#endregion

	#region SNEFRU algorithms

	public const SNEFRU = 'snefru';
	public const SNEFRU_256 = 'snefru256';

	#endregion

	#region TIGER algorithms

	public const TIGER128_3 = 'tiger128,3';
	public const TIGER160_3 = 'tiger160,3';
	public const TIGER192_3 = 'tiger192,3';
	public const TIGER128_4 = 'tiger128,4';
	public const TIGER160_4 = 'tiger160,4';
	public const TIGER192_4 = 'tiger192,4';

	#endregion

	#region Other algorithms

	public const ADLER32 = 'adler32';
	public const JOAAT = 'joaat';
	public const WHIRLPOOL = 'whirlpool';

	#endregion
}