<?php

namespace Andromeda\Util;

/**
 * Class ArrayUtil
 * @author  MartijnW
 * @package Andromeda\Util
 * @since   0.0.1
 */
class ArrayUtil
{
	/**
	 * Faster array_merge
	 *
	 * @author MartijnW
	 * @param array $source
	 * @param array ...$arrays
	 * @since  0.0.1
	 */
	public static function fastMerge(array &$source, array &...$arrays)
	{
		foreach ($arrays as $array)
			foreach ($array as $item)
				$source[] = $item;
	}
}