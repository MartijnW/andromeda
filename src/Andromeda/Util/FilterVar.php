<?php
declare(strict_types=1);

namespace Andromeda\Util;

/**
 * Class FilterVar
 *
 * @author  MartijnW
 * @package Andromeda\Util
 * @since   1.0.1
 */
final class FilterVar
{
	#region Parse

	/**
	 * Tries to parse a boolean, returns null on failure
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param $input
	 * @return bool|null
	 */
	public static function parseBoolean($input) : ?bool
	{
		return filter_var($input, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
	}

	#endregion

	#region Validate

	/**
	 * Tries to parse a boolean, null on failure
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param mixed $input
	 * @return bool
	 */
	public static function validBoolean($input) : bool
	{
		return filter_var($input, FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) !== null;
	}

	/**
	 * Checks if the given input email is valid
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param string $input
	 * @return bool
	 */
	public static function validEmail(string $input) : bool
	{
		$input = self::sanitizeEmail($input);

		return filter_var($input, FILTER_VALIDATE_EMAIL) !== false;
	}

	/**
	 * Checks if the given domain is valid
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param mixed $input
	 * @param bool  $hostname
	 * @return bool
	 */
	public static function validDomain($input, bool $hostname = false) : bool
	{
		if ($hostname)
			return filter_var($input, FILTER_VALIDATE_DOMAIN, FILTER_FLAG_HOSTNAME) !== false;

		return filter_var($input, FILTER_VALIDATE_DOMAIN) !== false;
	}

	/**
	 * Validates the given float
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param mixed $input
	 * @param bool  $allow_thousand
	 * @return bool
	 */
	public static function validFloat($input, bool $allow_thousand = false) : bool
	{
		$options = 0;

		if ($allow_thousand)
			$options |= FILTER_FLAG_ALLOW_THOUSAND;

		return filter_var($input, FILTER_VALIDATE_FLOAT, $options) !== false;
	}

	/**
	 * Validates the given int
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param mixed $input
	 * @param bool  $allow_octal
	 * @param bool  $allow_hex
	 * @return bool
	 */
	public static function validInt($input, bool $allow_octal = false, bool $allow_hex = false) : bool
	{
		if ($allow_octal && $allow_hex)
			return filter_var($input, FILTER_VALIDATE_INT, FILTER_FLAG_ALLOW_OCTAL | FILTER_FLAG_ALLOW_HEX) !== false;

		if ($allow_octal)
			return filter_var($input, FILTER_VALIDATE_INT, FILTER_FLAG_ALLOW_OCTAL) !== false;

		if ($allow_hex)
			return filter_var($input, FILTER_VALIDATE_INT, FILTER_FLAG_ALLOW_HEX) !== false;

		return filter_var($input, FILTER_VALIDATE_INT) !== false;
	}

	/**
	 * Validates the given IP
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param mixed $input
	 * @param bool  $no_private_range
	 * @param bool  $no_reserved_range
	 * @return bool
	 */
	public static function validIp($input, bool $no_private_range = false, bool $no_reserved_range = false) : bool
	{
		if ($no_private_range && $no_reserved_range)
			return filter_var($input, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false;

		if ($no_private_range)
			return filter_var($input, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE) !== false;

		if ($no_reserved_range)
			return filter_var($input, FILTER_VALIDATE_IP, FILTER_FLAG_NO_RES_RANGE) !== false;

		return filter_var($input, FILTER_VALIDATE_IP) !== false;
	}

	/**
	 * Validates the given IPv4
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param mixed $input
	 * @param bool  $no_private_range
	 * @param bool  $no_reserved_range
	 * @return bool
	 */
	public static function validIpv4($input, bool $no_private_range = false, bool $no_reserved_range = false) : bool
	{
		if ($no_private_range && $no_reserved_range)
			return filter_var($input, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE | FILTER_FLAG_IPV4) !== false;

		if ($no_private_range)
			return filter_var($input, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_IPV4) !== false;

		if ($no_reserved_range)
			return filter_var($input, FILTER_VALIDATE_IP, FILTER_FLAG_NO_RES_RANGE | FILTER_FLAG_IPV4) !== false;

		return filter_var($input, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4) !== false;
	}

	/**
	 * Validates the given IPv6
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param mixed $input
	 * @param bool  $no_private_range
	 * @param bool  $no_reserved_range
	 * @return bool
	 */
	public static function validIpv6($input, bool $no_private_range = false, bool $no_reserved_range = false) : bool
	{
		if ($no_private_range && $no_reserved_range)
			return filter_var($input, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE | FILTER_FLAG_IPV6) !== false;

		if ($no_private_range)
			return filter_var($input, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_IPV6) !== false;

		if ($no_reserved_range)
			return filter_var($input, FILTER_VALIDATE_IP, FILTER_FLAG_NO_RES_RANGE | FILTER_FLAG_IPV6) !== false;

		return filter_var($input, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6) !== false;
	}

	/**
	 * Validates the given MAC address
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param mixed $input
	 * @return bool
	 */
	public static function validMacAddress($input) : bool
	{
		return filter_var($input, FILTER_VALIDATE_MAC) !== false;
	}

	/**
	 * Validates the given regular expression (RegExp)
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param mixed $input
	 * @return bool
	 */
	public static function validRegularExpression($input) : bool
	{
		return filter_var($input, FILTER_VALIDATE_REGEXP) !== false;
	}

	/**
	 * Validates the given url
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param mixed $input
	 * @param bool  $require_host
	 * @param bool  $require_scheme
	 * @param bool  $require_path
	 * @param bool  $require_query
	 * @return bool
	 */
	public static function validUrl($input, bool $require_host = false, bool $require_scheme = false, bool $require_path = false, bool $require_query = false) : bool
	{
		$options = 0;

		if ($require_host)
			$options |= FILTER_FLAG_HOST_REQUIRED;

		if ($require_scheme)
			$options |= FILTER_FLAG_SCHEME_REQUIRED;

		if ($require_path)
			$options |= FILTER_FLAG_PATH_REQUIRED;

		if ($require_query)
			$options |= FILTER_FLAG_QUERY_REQUIRED;

		return filter_var($input, FILTER_VALIDATE_URL, $options) !== false;
	}

	#endregion

	#region Sanitize

	/**
	 * Removes all invalid characters from the given email
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param string $input
	 * @return string
	 */
	public static function sanitizeEmail(string $input) : string
	{
		return filter_var($input, FILTER_SANITIZE_EMAIL);
	}

	/**
	 * Sanitizes the string, similair to urlencode()
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param string $input
	 * @param bool   $strip_backtick
	 * @param bool   $strip_low
	 * @param bool   $strip_high
	 * @param bool   $encode_low
	 * @param bool   $encode_high
	 * @return null|string
	 */
	public static function sanitizeEncoded(string $input, bool $strip_backtick = false, bool $strip_low = false, bool $strip_high = false, bool $encode_low = false, bool $encode_high = false) : ?string
	{
		$options = 0;

		if ($strip_backtick)
			$options |= FILTER_FLAG_STRIP_BACKTICK;

		if ($strip_low)
			$options |= FILTER_FLAG_STRIP_LOW;

		if ($strip_high)
			$options |= FILTER_FLAG_STRIP_HIGH;

		if ($encode_low)
			$options |= FILTER_FLAG_ENCODE_LOW;

		if ($encode_high)
			$options |= FILTER_FLAG_ENCODE_HIGH;

		return filter_var($input, FILTER_SANITIZE_ENCODED, $options);
	}

	/**
	 * Apply addslashes()
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param string $input
	 * @return string
	 */
	public static function sanitizeMagicQuotes(string $input) : string
	{
		return filter_var($input, FILTER_SANITIZE_MAGIC_QUOTES);
	}

	/**
	 * Sanitizes the given float by removing all characters except digits, '+', '-' and optionally '.', ',', 'e' and 'E'
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param mixed $input
	 * @param bool  $allow_thousand
	 * @param bool  $allow_fraction
	 * @param bool  $allow_scientific
	 * @return string
	 */
	public static function sanitizeFloat($input, bool $allow_thousand, bool $allow_fraction = false, bool $allow_scientific = false) : string
	{
		$options = 0;

		if ($allow_fraction)
			$options |= FILTER_FLAG_ALLOW_FRACTION;

		if ($allow_thousand)
			$options |= FILTER_FLAG_ALLOW_THOUSAND;

		if ($allow_scientific)
			$options |= FILTER_FLAG_ALLOW_SCIENTIFIC;

		return filter_var($input, FILTER_SANITIZE_NUMBER_FLOAT, $options);
	}

	/**
	 * Sanitizes the given int by removing all characters except digits, '+' and '-'
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param $input
	 * @return string
	 */
	public static function sanitizeInt($input) : string
	{
		return filter_var($input, FILTER_SANITIZE_NUMBER_INT);
	}

	/**
	 * HTML-escape
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param string $input
	 * @param bool   $strip_backtick
	 * @param bool   $strip_low
	 * @param bool   $strip_high
	 * @param bool   $encode_high
	 * @return null|string
	 */
	public static function sanitizeSpecialChars(string $input, bool $strip_backtick = false, bool $strip_low = false, bool $strip_high = false, bool $encode_high = false) : ?string
	{
		$options = 0;

		if ($strip_backtick)
			$options |= FILTER_FLAG_STRIP_BACKTICK;

		if ($strip_low)
			$options |= FILTER_FLAG_STRIP_LOW;

		if ($strip_high)
			$options |= FILTER_FLAG_STRIP_HIGH;

		if ($encode_high)
			$options |= FILTER_FLAG_ENCODE_HIGH;

		return filter_var($input, FILTER_SANITIZE_SPECIAL_CHARS, $options);
	}

	/**
	 * Similair to htmlspecialchars()
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param string $input
	 * @param bool   $allow_quotes
	 * @return string
	 */
	public static function sanitizeFullSpecialChars(string $input, bool $allow_quotes = false) : string
	{
		$options = 0;

		if ($allow_quotes)
			$options |= FILTER_FLAG_NO_ENCODE_QUOTES;

		return filter_var($input, FILTER_SANITIZE_FULL_SPECIAL_CHARS, $options);
	}

	/**
	 * Sanitizes the given string
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param string $input
	 * @param bool   $strip_backtick
	 * @param bool   $strip_low
	 * @param bool   $strip_high
	 * @param bool   $encode_low
	 * @param bool   $encode_high
	 * @param bool   $encode_ampersand
	 * @param bool   $allow_quotes
	 * @return string
	 */
	public static function sanitizeString(string $input, bool $strip_backtick = false, bool $strip_low = false, bool $strip_high = false, bool $encode_low = false, bool $encode_high = false, bool $encode_ampersand = false, bool $allow_quotes = false) : string
	{
		$options = 0;

		if ($strip_backtick)
			$options |= FILTER_FLAG_STRIP_BACKTICK;

		if ($strip_low)
			$options |= FILTER_FLAG_STRIP_LOW;

		if ($strip_high)
			$options |= FILTER_FLAG_STRIP_HIGH;

		if ($encode_low)
			$options |= FILTER_FLAG_ENCODE_LOW;

		if ($encode_high)
			$options |= FILTER_FLAG_ENCODE_HIGH;

		if ($encode_ampersand)
			$options |= FILTER_FLAG_ENCODE_AMP;

		if ($allow_quotes)
			$options |= FILTER_FLAG_NO_ENCODE_QUOTES;

		return filter_var($input, FILTER_SANITIZE_STRING, $options);
	}

	/**
	 * Sanitizes the given url
	 *
	 * @since  1.0.1
	 * @author MartijnW
	 * @param string $input
	 * @return string
	 */
	public static function sanitizeUrl(string $input) : string
	{
		return filter_var($input, FILTER_SANITIZE_URL);
	}

	#endregion
}