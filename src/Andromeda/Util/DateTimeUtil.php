<?php
/**
 * Created by PhpStorm.
 * User: Martijn
 * Date: 21/03/2018
 * Time: 21:00
 */

namespace Andromeda\Util;

use DateTime;

/**
 * Class DateTimeUtil
 * Contains handy functions for working with dates and time.
 *
 * @author  MartijnW
 * @package Andromeda\Util
 * @since   1.0.0
 */
class DateTimeUtil
{
	/**
	 * Checks if the given string is conform the format
	 *
	 * @param string $dateTime
	 * @param string $format
	 * @author MartijnW
	 * @return bool
	 * @since  1.0.0
	 */
	public static function validDateTime(string $dateTime, string $format = 'Y-m-d H:i:s') : bool
	{
		$d = DateTime::createFromFormat($format, $dateTime);

		return $d && $d->format($format) == $dateTime;
	}
}