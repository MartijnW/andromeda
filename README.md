# `Andromeda\*`

Handy tools for PHP projects.

## Requirements
Andromeda requires PHP version >= 7.2 (7.1 support untested).

To use `Andromeda\Database` the PHP extension 
[PDO](http://php.net/manual/en/pdo.setup.php) should be installed and configured.  
**Note:** for Microsoft SQL connections, either sqlsrv or dlib should be installed.

## What's available?

At the moment Andromeda is still under heavy development and should not 
be used in production enviroments.

The following packages are available:
- `Andromeda\Data`
  - `Curl`
    - `CurlRequest`
  - `Xml` (WIP - not stable)
    - `XmlAttribute`
    - `XmlDocument`
    - `XmlElement`
- `Andromeda\Database`
  - `DatabaseModel`
  - `MsSql`
    - `MsSqlCredentials`
    - `MsSqlManager`
  - `MySql`
    - `MySqlCredentials`
    - `MySqlManager`
  - `SqLite`
    - `SqLiteManager`
- `Andromeda\Synology` (WIP - not stable)
  - `DiskStation`
  - `SynologyCredentials`
- `Andromeda\Util`
  - `ArrayUtil`
  - `CurlUtil`
  - `DateTimeUtil`
  - `FilterVar`
  - `HttpCodes`
  - `IOUtil`
  - `ReflectionUtil`
  - `RuntimeUtil`
  - `StringUtil`